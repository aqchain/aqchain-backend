module gitee.com/aqchain/backend

go 1.16

require (
	gitee.com/aqchain/go-ethereum v0.0.6
	github.com/fogleman/gg v1.3.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.1
	github.com/smartwalle/alipay/v3 v3.1.6
	github.com/spf13/viper v1.8.1
	github.com/swaggest/openapi-go v0.2.10
	github.com/ugorji/go v1.1.13 // indirect
	github.com/x-mod/routine v1.3.3
	golang.org/x/crypto v0.0.0-20210415154028-4f45737414dc // indirect
	golang.org/x/image v0.0.0-20210220032944-ac19c3e999fb // indirect
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.7
)

replace gitee.com/aqchain/go-ethereum v0.0.6 => E:\Code\newest_anquanlian\go-ethereum-v2
