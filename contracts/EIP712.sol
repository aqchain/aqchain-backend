
pragma solidity 0.5.4;

import './cryptography/ECDSA.sol';

/** 
 * @dev 结构体签名与验证
 */
contract EIP712 {
    
    using ECDSA for bytes32;
    
    struct EIP712Domain {
        string  name;
        string  version;
        uint256 chainId;
        address verifyingContract;
    }
    
    struct File {
        string fileHash;
        address creator;
        string tokenURI;
    }
    
    
    bytes32 constant EIP712DOMAIN_TYPEHASH = keccak256(
        "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)"
    );

    bytes32 constant FILE_TYPEHASH = keccak256(
        "File(string fileHash,address creator,string tokenURI)"
    );
    
    
    bytes32 DOMAIN_SEPARATOR;

    constructor () public {
        DOMAIN_SEPARATOR = hash(EIP712Domain({
            name: "AQChain",
            version: '1',
            chainId: 9999,
            verifyingContract: address(this)
        }));
    }
    
    function hash(EIP712Domain memory eip712Domain) internal pure returns (bytes32) {
        return keccak256(abi.encode(
            EIP712DOMAIN_TYPEHASH,
            keccak256(bytes(eip712Domain.name)),
            keccak256(bytes(eip712Domain.version)),
            eip712Domain.chainId,
            eip712Domain.verifyingContract
        ));
    }
    
    function hash(File memory file) internal pure returns (bytes32) {
        return keccak256(abi.encode(
            FILE_TYPEHASH,
            keccak256(bytes(file.fileHash)),
            file.creator,
            keccak256(bytes(file.tokenURI))
        ));
    }

    
    function verify(File memory file, bytes memory signature) internal view returns (bool) {
        bytes32 digest = keccak256(abi.encodePacked(
            "\x19\x01",
            DOMAIN_SEPARATOR,
            hash(file)
        ));
        return digest.recover(signature) == file.creator;
    }
    
    
    function test(string memory fileHash,address creator,string memory tokenURI,bytes memory signature) public view returns(bool){
        File memory file = File(fileHash,creator,tokenURI);
        return verify(file,signature);
    }
}