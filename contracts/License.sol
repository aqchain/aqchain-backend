pragma solidity 0.5.4;

import './utils/Context.sol';
import './utils/ReentrancyGuard.sol';
import './utils/Address.sol';
import './Counters.sol';
import './token/ERC721/ERC721.sol';
import './token/ERC20/ERC20.sol';

/** 
 * @dev 授权合约
 */
contract License is Context,ReentrancyGuard{
    
    using Address for address payable;
    using Counters for Counters.Counter;
    
    /** 
     * @dev 授权设置
     */
    struct LicenseSet{
        address payable creator;
        uint256 price;
        uint256 maxCount;
        uint256 count;
    }
    
    /** 
     * @dev 授权记录
     */
    struct LicenseContent{
        address from;
        address to;
        uint256 price;
        uint256 createTiem;
    }
    
    /** 
     * @dev 非同质代币合约实例 对应作品确权合约
     */
    ERC721 private _erc721Contract;
    
    /** 
     * @dev 同质代币合约实例 对应安权链代币合约 todo 未完成代币合约
     */
    ERC20 private _erc20Contract;
    
    /** 
     * @dev 授权记录计数器
     */
    Counters.Counter private _licenseIds;
    
    /** 
     * @dev 授权交易设置集合
     */
    mapping(uint256=>LicenseSet) private _licenseSets;
    
    /** 
     * @dev 授权记录集合 licenseId=>LicenseContent
     */
    mapping(uint256=>LicenseContent) private _licenses;
    
    /** 
     * @dev 授权记录集合 tokenId=>(account=>licenseId)
     */
    mapping(uint256=>mapping(address=>uint256)) private _licensesToAccount;
    
    constructor(address erc721Contract) public{
        _erc721Contract = ERC721(erc721Contract);
    }
    
    /** 
     * @dev 授权创建事件
     */
    event Create(uint256 indexed tokenId,uint256 price,uint256 maxCount);
    
    /** 
     * @dev 授权结束事件
     */
    event Finish(uint256 indexed tokenId,uint256 price,uint256 maxCount,uint256 count);
    
    /** 
     * @dev 购买授权事件
     */
    event Buy(uint256 indexed tokenId,uint256 price,uint256 maxCount,uint256 count,uint256 licenseId);
    
    /** 
     * @dev 获取授权记录id
     */
    function getLicenseId(uint256 tokenId,address account) public view returns(uint256){
        return _licensesToAccount[tokenId][account];
    }
    
    /** 
     * @dev 创建交易 需要预先授权token操作权限 创建交易后token的所有权会转移到本合约
     */
    function create(uint256 tokenId,uint256 price,uint256 maxCount) external{
        // 检查token是否正在交易
        require(_licenseSets[tokenId].creator==address(0),"License: token is on license");
        // 将token所有权转移给本合约
        _erc721Contract.transferFrom(_msgSender(),address(this),tokenId);
        // 创建交易信息
        _licenseSets[tokenId] = LicenseSet(_msgSender(),price,maxCount,0);
        emit Create(tokenId,price,maxCount);
    }
    
    /** 
     * @dev 结束交易 只能由创建者调用 结束交易后token的所有权会转移回创建者
     */
    function finish(uint256 tokenId) external{
        LicenseSet memory licenseSet = _licenseSets[tokenId];
        // 检查token是否正在交易
        require(licenseSet.creator!=address(0),"License: token is not on license");
        // 检查调用者是否为创建者
        require(licenseSet.creator==_msgSender(),"License : caller is not creator");
        // 返还token所有权
        _erc721Contract.transferFrom(address(this),_msgSender(),tokenId);
        // 删除交易信息
        delete _licenseSets[tokenId];
        emit Finish(tokenId,licenseSet.price,licenseSet.maxCount,licenseSet.count);
    }
    
    /** 
     * @dev 购买授权
     */
    function buy(uint256 tokenId) nonReentrant external payable returns(uint256){
        LicenseSet memory licenseSet = _licenseSets[tokenId];
        // 检查token是否正在交易
        require(licenseSet.creator!=address(0),"License: token is not on license");
        // 检查授权数量是否已经到达最大值
        require(licenseSet.count<licenseSet.maxCount,"License: license is on max count");
        // 检查发送的以太币数量
        require(licenseSet.price<=msg.value,"License: not enough value");
        // 创建授权记录
        _licenseIds.increment();
        uint256 newLicenseId = _licenseIds.current();
        _licenses[newLicenseId] = LicenseContent(licenseSet.creator,_msgSender(),licenseSet.price,now);
        _licensesToAccount[tokenId][_msgSender()] = newLicenseId;
        licenseSet.count++;
        // 向创建者发送以太币
        licenseSet.creator.sendValue(licenseSet.price);
        emit Buy(tokenId,licenseSet.price,licenseSet.maxCount,licenseSet.count,newLicenseId);
    }
}