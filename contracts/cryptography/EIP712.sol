// SPDX-License-Identifier: MIT

pragma solidity 0.5.4;

import "./ECDSA.sol";

contract EIP712 {
    
    struct EIP712Domain {
        string  name;
        string  version;
        uint256 chainId;
        address verifyingContract;
    }
    
    bytes32 constant EIP712DOMAIN_TYPEHASH = keccak256(
        "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)"
    );

    bytes32 constant FILE_TYPEHASH = keccak256(
        "File(string fileHash,address creator,string tokenURI)"
    );

    bytes32 DOMAIN_SEPARATOR;

    constructor (string memory name, string memory version,uint256 chinid) public {
        DOMAIN_SEPARATOR = hash(EIP712Domain({
            name: name,
            version: version,
            chainId: chainid,
            verifyingContract: address(this)
        }));
    }
    
    function hash(EIP712Domain memory eip712Domain) internal pure returns (bytes32) {
        return keccak256(abi.encode(
            EIP712DOMAIN_TYPEHASH,
            keccak256(bytes(eip712Domain.name)),
            keccak256(bytes(eip712Domain.version)),
            eip712Domain.chainId,
            eip712Domain.verifyingContract
        ));
    }

    function domainSeparator() internal view returns (bytes32) {
        return DOMAIN_SEPARATOR;
    }

    function hashTypedData(bytes32 structHash) internal view returns (bytes32) {
        return ECDSA.toTypedDataHash(DOMAIN_SEPARATOR, structHash);
    }
}