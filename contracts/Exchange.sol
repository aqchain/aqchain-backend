
pragma solidity 0.5.4;

import "./utils/Address.sol";
import "./math/SafeMath.sol";
import "./utils/Context.sol";
import "./utils/ReentrancyGuard.sol";
import "./access/AccessControl.sol";
import "./AQToken.sol";


/** 
 * @dev 以太币与法币兑换合约，完整兑换过程需要后台程序配合完成
 */
contract Exchange is Context,AccessControl,ReentrancyGuard{
    
    using Address for address payable;
    using SafeMath for uint256;
    
    /** 
     * @dev 定义合约执行权限角色
     */
    bytes32 public constant EXEC_ROLE = keccak256("EXEC_ROLE");
    
    /** 
     * @dev 小数位数控制 默认18位对应1ether
     */
    uint256 private _decimals = 18;
    
     /** 
     * @dev 最小的充值和提现额度 默认是0.01ether
     */
    uint256 private _min = 10 ** (_decimals-2);
    
    AQToken private _aqtoken;
    
    modifier onlyExecutor(){
        require(hasRole(EXEC_ROLE,_msgSender()), "Caller is not a executor");
        _;
    }
    
    /** 
     * @dev 创建合约时设置合约的管理员等权限
     */
    constructor(address aqtoken,address[] memory executors) public payable{
        _aqtoken = AQToken(aqtoken);
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _setupRole(EXEC_ROLE,_msgSender());
        for(uint8 i=0;i<executors.length;i++){
            _setupRole(EXEC_ROLE,executors[i]);
        }
    }
    
    /** 
     * @dev 法币兑换以太币事件
     */
    event CurrencyToEth(uint256 amount,address indexed account,string orderNo);
    /** 
     * @dev 以太币兑换法币事件
     */
    event EthToCurrency(uint256 amount,address indexed account,string orderNo);
    
    /** 
     * @dev 获取小数位数
     */
    function decimals() public view returns(uint256){
        return _decimals;
    }
    
    /** 
     * @dev 获取最小额度
     */
    function min() public view returns(uint256){
        return _min;
    }
    
    /** 
     * @dev 向合约存储以太币
     */
    function deposit() onlyExecutor public payable{
        require(msg.value>=_min,"Exchange: amount less than min");
    }
    
    /** 
     * @dev 合约余额提取
     */
    function withdraw(uint256 amount) onlyExecutor nonReentrant public{
        require(address(this).balance>=amount,"Exchange: balance not enough");
        _msgSender().sendValue(amount);
    }

    /** 
     * @dev 法币兑换以太币
     */
    function currencyToEth(address payable account,uint256 amount,string memory orderNo) onlyExecutor nonReentrant public{
        require(account!=address(0),"Exchange: account cant be null");
        require(amount>=_min,"Exchange: amount less than min");
        account.sendValue(amount);
        emit CurrencyToEth(amount,account,orderNo);
    }
    
    /** 
     * @dev 以太币兑换法币
     */
    function ethToCurrency(string memory orderNo) public payable{
        require(msg.value>_min,"Exchange: amount less than min");
        emit EthToCurrency(msg.value,_msgSender(),orderNo);
    }
    
}