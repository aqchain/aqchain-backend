
pragma solidity 0.5.4;

import './utils/Context.sol';
import './utils/ReentrancyGuard.sol';
import './utils/Address.sol';
import './math/SafeMath.sol';
import './token/ERC721/ERC721.sol';
import './token/ERC20/ERC20.sol';

/** 
 * @dev 拍卖交易合约
 */
contract Auction is Context,ReentrancyGuard{
    
    using Address for address payable;
    using SafeMath for uint256;
    
    /** 
     * @dev 拍卖设置
     */
    struct AuctionSet{
        address payable seller;
        uint256 lowestPrice;
        uint256 begin;
        uint256 end;
        address payable highestBidder;
        uint256 highestBid;
    }
    
    /** 
     * @dev 非同质代币合约实例 对应作品确权合约
     */
    ERC721 private _erc721Contract;
    
    /** 
     * @dev 同质代币合约实例 对应安权链代币合约 todo 未完成代币合约
     */
    ERC20 private _erc20Contract;
    
    /** 
     * @dev 交易信息集合
     */
    mapping(uint256=>AuctionSet) private _auctions;
    
    constructor(address erc721Contract) public{
        _erc721Contract = ERC721(erc721Contract);
    }
    
    /** 
     * @dev 创建拍卖事件
     */
    event Create(uint256 indexed tokenId,address seller,uint256 price,uint256 begin,uint256 end);
    
    /** 
     * @dev 出价事件
     */
    event Bid(uint256 indexed tokenId,address bider,uint256 bid);
    
    /** 
     * @dev 结束拍卖事件
     */
    event Finish(uint256 indexed tokenId,address seller,uint256 highestBid,address highestBidder);
    
    /** 
     * @dev 获取卖方地址
     */
    function getSeller(uint256 tokenId) external view returns(address){
        return _auctions[tokenId].seller;
    }
    
    /** 
     * @dev 获取起拍价格
     */
    function getLowestPrice(uint256 tokenId) external view returns(uint256){
        return _auctions[tokenId].lowestPrice;
    }
    
    /** 
     * @dev 获取结束时间
     */
    function getEnd(uint256 tokenId) external view returns(uint256){
        return _auctions[tokenId].end;
    }
    
    /** 
     * @dev 获取最高出价者地址
     */
    function getHighestBidder(uint256 tokenId) external view returns(address){
        return _auctions[tokenId].highestBidder;
    }
    
    /** 
     * @dev 获取最高出价
     */
    function getHighestBid(uint256 tokenId) external view returns(uint256){
        return _auctions[tokenId].highestBid;
    }
    
    /** 
     * @dev 创建拍卖 需要预先授权token操作权限 创建交易后token的所有权会转移到本合约
     */
    function create(uint256 tokenId,uint256 price,uint256 duration) external{
        // 检查token是否正在交易
        require(_auctions[tokenId].seller==address(0),"Auction: token is on auction");
        // 将token所有权转移给本合约
        _erc721Contract.transferFrom(_msgSender(),address(this),tokenId);
        // 创建交易信息
        uint256 _end = now.add(duration);
        _auctions[tokenId] = AuctionSet(_msgSender(),price,now,_end,address(0),0);
        emit Create(tokenId,_msgSender(),price,now,_end);
    }
    
    function createTest(uint256 tokenId,uint256 price,uint256 duration) external view returns(uint256){
        // 创建交易信息
        uint256 _end = now.add(duration);
        return _end;
    }
    
    /** 
     * @dev 出价 需要向合约质押以太币 等待拍卖结束结算
     */
    function bid(uint256 tokenId) nonReentrant external payable{
        AuctionSet storage auction = _auctions[tokenId];
        // 检查token是否正在交易
        require(auction.seller!=address(0),"Auction: token is not on auction");
        // 检查拍卖时间是否已经结束
        require(auction.end>now,"Auction: auction is ended");
        // 检查以太币发送数量是否大于起拍价格
        require(auction.lowestPrice<=msg.value,"Auction: bid can not lower than the lowest price");
        // 当已经存在出价时 检查发送的以太坊币数量是否大于最高出价
        if(auction.highestBidder!=address(0)){
            require(auction.highestBid<msg.value,"Auction: bid can not lower than the highest bid");
            // 返还前一个最高出价者质押的以太币
            auction.highestBidder.sendValue(auction.highestBid);
        }
        // 修改最高出价状态
        auction.highestBid = msg.value;
        auction.highestBidder = _msgSender();
        _auctions[tokenId]=auction;
        emit Bid(tokenId,_msgSender(),msg.value);
    }
    
    /** 
     * @dev 结束拍卖 需要等待拍卖时间结束后执行 完成token和以太币结算
     */
    function finish(uint256 tokenId) nonReentrant external{
        AuctionSet memory auction = _auctions[tokenId];
        // 检查token是否正在交易
        require(auction.seller!=address(0),"Auction: token is not on auction");
        // 检查拍卖时间是否已经结束
        require(auction.end<=now,"Auction: auction is not ended yet");
        // 当存在最高出价时 转移token所有权给出价者 发送以太币给卖方
        if(auction.highestBidder!=address(0)){
            _erc721Contract.transferFrom(address(this),auction.highestBidder,tokenId);
            auction.seller.sendValue(auction.highestBid);
        }
        // 删除交易信息
        delete _auctions[tokenId];
        emit Finish(tokenId,auction.seller,auction.highestBid,auction.highestBidder);
    }
    
}