
pragma solidity 0.5.4;

import './utils/Counters.sol';
import './token/ERC721/ERC721.sol';
import './EIP712.sol';

/** 
 * @dev 作品内容确权合约
 */
contract FileStorage is ERC721,EIP712{
    
    using Counters for Counters.Counter;
    
    /** 
     * @dev token计数器
     */
    Counters.Counter private _tokenIds;
    
    /** 
     * @dev fileHash对应的token
     */
    mapping(string=>uint256) private _fileHashs;
    
    /** 
     * @dev 记录token的创建者
     */
    mapping(uint256=>address) private _creators;
    
    /** 
     * @dev 创建token/作品确权事件
     */
    event Create(string fileHash, uint256 indexed tokenId,address indexed creator, string tokenURI);
    
    constructor() public ERC721("AQChain", "AQC") {
        // // 创建合约时将计数器加1 确保tokenId从1开始
        // _tokenIds.increment();
    }
    
    /** 
     * @dev 作品确权
     */
    function createTest(string memory fileHash,address creator,string memory tokenURI) public{
        
        require(_fileHashs[fileHash] == 0, "FileStorage: existent fileHash");
        
        _tokenIds.increment();
        uint256 newItemId = _tokenIds.current();
        _mint(creator, newItemId);
        _setTokenURI(newItemId, tokenURI);
        
         _fileHashs[fileHash] = newItemId;
         _creators[newItemId] = creator;
         
        emit Create(fileHash,newItemId,creator,tokenURI);
    }
    
    /** 
     * @dev 作品确权 使用签名验证方式 目的是区分作品的创建者和区块链的调用者
     */
    function create(string memory fileHash,address creator,string memory tokenURI,bytes memory signature) public{
        
        // 检查fileHash是否已经存在
        require(_fileHashs[fileHash] == 0, "FileStorage: existent fileHash");
        // 检查签名 参考EIP712关于结构化签名的详细
        File memory file = File(fileHash,creator,tokenURI);
        require(verify(file,signature),"FileStorage: sign error");
        // 创建新的token
        _tokenIds.increment();
        uint256 newItemId = _tokenIds.current();
        _mint(creator, newItemId);
        _setTokenURI(newItemId, tokenURI);
        // 记录数据
         _fileHashs[fileHash] = newItemId;
         _creators[newItemId] = creator;
        emit Create(fileHash,newItemId,creator,tokenURI);
    }
    
    /** 
     * @dev 授权作品操作权 使用签名验证方式
     */
    // function permit(address owner,address to, uint256 tokenId,bytes memory signature) public {
    //     require(ownerOf(tokenId) == owner, "ERC721: owner param error");
    //     require(to != owner, "ERC721: approval to current owner");
    //     Permit memory _permit = Permit(owner,to,tokenId);
    //     require(verify(_permit,signature),"ERC721: sign error");
    //     approve(to,tokenId);
    // } 
    
    function getTokenId(string memory fileHash) public view returns (uint256){
        return _fileHashs[fileHash];
    }
    
}