
pragma solidity 0.5.4;

import "./access/AccessControl.sol";
import "./token/ERC20/ERC20.sol";
import "./math/SafeMath.sol";

/** 
 * @dev ERC20代币合约
 */
contract AQToken is ERC20,AccessControl{
    
    using SafeMath for uint256;
    
    /** 
     * @dev 增发新代币权限角色
     */
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    /** 
     * @dev 合约管理权限角色
     */
    bytes32 public constant EXEC_ROLE = keccak256("EXEC_ROLE");
    
    constructor(address[] memory minters,address[] memory executors) ERC20("AQToken", "AQT") public {
        
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _setupRole(EXEC_ROLE,_msgSender());
        _setupRole(MINTER_ROLE, _msgSender());
        
         for(uint8 i=0;i<minters.length;i++){
            _setupRole(MINTER_ROLE,minters[i]);
        }
        
        for(uint8 i=0;i<executors.length;i++){
            _setupRole(EXEC_ROLE,executors[i]);
        }
    }
    
    function mint(address to, uint256 amount) public {
        require(hasRole(MINTER_ROLE, _msgSender()), "Caller is not a minter");
        _mint(to, amount);
    }
    
    function exchange(uint256 amount) payable public{
        
    }
    
}