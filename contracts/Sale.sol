
pragma solidity 0.5.4;

import './utils/Context.sol';
import './utils/ReentrancyGuard.sol';
import './utils/Address.sol';
import './token/ERC721/ERC721.sol';
import './token/ERC20/ERC20.sol';

/** 
 * @dev 定价交易合约
 */
contract Sale is Context,ReentrancyGuard{
    
    using Address for address payable;
    
    /** 
     * @dev 定价交易设置
     */
    struct SaleSet{
        address payable seller;
        uint256 price;
        address buyer;
    }
    
    /** 
     * @dev 非同质代币合约实例 对应作品确权合约
     */
    ERC721 private _erc721Contract;
    
    /** 
     * @dev 同质代币合约实例 对应安权链代币合约 todo 未完成代币合约
     */
    ERC20 private _erc20Contract;
    
    /** 
     * @dev 交易信息集合
     */
    mapping(uint256=>SaleSet) private _sales;
    
    /** 
     * @dev 交易创建事件
     */
    event Create(uint256 indexed tokenId,uint256 price,address seller,address buyer);
    
    /** 
     * @dev 交易取消事件
     */
    event Cancel(uint256 indexed tokenId,uint256 price,address seller,address buyer);
    
    /** 
     * @dev 交易确认事件
     */
    event Confirm(uint256 indexed tokenId,uint256 price,address seller,address buyer);
    
    constructor(address erc721Contract) public{
        _erc721Contract = ERC721(erc721Contract);
    }
    
    /** 
     * @dev 获取卖方地址
     */
    function getSeller(uint256 tokenId) public view returns(address){
        return _sales[tokenId].seller;
    }
    
    /** 
     * @dev 获取价格
     */
    function getPrice(uint256 tokenId) public view returns(uint256){
        return _sales[tokenId].price;
    }
    
    /** 
     * @dev 获取预先指定买方地址
     */
    function getBuyer(uint256 tokenId) public view returns(address){
        return _sales[tokenId].buyer;
    }
    
    /** 
     * @dev 创建交易 需要预先授权token操作权限 创建交易后token的所有权会转移到本合约
     */
    function create(uint256 tokenId,uint256 price,address buyer) public{
        // 检查token是否正在交易
        require(_sales[tokenId].seller==address(0),"Sale: token is on sale");
        // 将token所有权转移给本合约
        _erc721Contract.transferFrom(_msgSender(),address(this),tokenId);
        // 创建交易信息
        _sales[tokenId] = SaleSet(_msgSender(),price,buyer);
        emit Create(tokenId,price,_msgSender(),buyer);
    }
    
    // /** 
    //  * @dev 使用签名创建交易 创建交易后token的所有权会转移到本合约
    //  */
    // function permit(uint256 tokenId,uint256 price,address buyer,uint256 duration,bytes memory signature) public{
    //     // 检查token是否正在交易
    //     require(_sales[tokenId].seller==address(0),"Sale: token is on sale");
    // }
    
    /** 
     * @dev 取消交易 只能由创建者调用 取消交易后token的所有权会转移回创建者
     */
    function cancel(uint256 tokenId) public{
        SaleSet memory sale = _sales[tokenId];
        // 检查token是否正在交易
        require(sale.seller!=address(0),"Sale: token is not on sale");
        // 检查调用者是否为交易创建者
        require(sale.seller==_msgSender(),"Sale: caller is not seller");
        // 返还token所有权
        _erc721Contract.transferFrom(address(this),_msgSender(),tokenId);
        // 删除交易信息
        delete _sales[tokenId];
        emit Cancel(tokenId,sale.price,sale.seller,sale.buyer);
    }
    
    /** 
     * @dev 确认交易 
     */
    function confirm(uint256 tokenId) nonReentrant public payable{
        SaleSet memory sale =  _sales[tokenId];
        // 检查token是否正在交易
        require(sale.seller!=address(0),"Sale: token is not on sale");
        // 检查以太币发送数量
        require(msg.value>=sale.price,"Sale: not enough value");
        // 如果已指定买方账户 检查调用者是否为指定账户
        if(sale.buyer!=address(0)){
            require(_msgSender()==sale.buyer,"Sale: caller is not appoint account");
        }
        // 转移token所有权
        _erc721Contract.transferFrom(address(this),_msgSender(),tokenId);
        // 向卖方发送以太币
        sale.seller.sendValue(sale.price);
        // 删除交易信息
        delete _sales[tokenId];
        emit Confirm(tokenId, sale.price, sale.seller, _msgSender());
    }
    
}