package database

import (
	"gitee.com/aqchain/backend/config"
	"gitee.com/aqchain/backend/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

// Database 获取数据库连接实例 进行所有关于gorm连接数据库的操作
// 1 gorm配置
// 2 连接数据库
// 3 开发环境中执行的操作
func Database(cfg config.DatabaseConfig) (*gorm.DB, error) {

	// gorm配置
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // 慢 SQL 阈值
			LogLevel:      logger.Info, // Log level
			Colorful:      false,       // 禁用彩色打印
		},
	)

	// 连接数据库
	db, err := gorm.Open(mysql.Open(cfg.DSN), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		return nil, err
	}

	if cfg.Develop {
		// 自动创建数据库表
		err = db.AutoMigrate(&models.Account{}, &models.Item{}, &models.FileStorage{}, &models.Sale{}, &models.Auction{}, &models.Block{}, &models.Transaction{}, &models.Deposit{}, &models.Withdraw{})
		if err != nil {
			return nil, err
		}
	}

	return db, nil
}
