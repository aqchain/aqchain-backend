package client

import (
	"gitee.com/aqchain/backend/config"
	"gitee.com/aqchain/go-ethereum/common"
	"gitee.com/aqchain/go-ethereum/contracts/auction"
	"gitee.com/aqchain/go-ethereum/contracts/exchange"
	"gitee.com/aqchain/go-ethereum/contracts/fileStorage"
	"gitee.com/aqchain/go-ethereum/contracts/license"
	"gitee.com/aqchain/go-ethereum/contracts/sale"
	"gitee.com/aqchain/go-ethereum/crypto"
	"gitee.com/aqchain/go-ethereum/ethclient"
	"gitee.com/aqchain/go-ethereum/rpc"
	"github.com/smartwalle/alipay/v3"
	"gorm.io/gorm"
)

// Client 用于连接区块链和获取区块链数据
type Client struct {
	ethClient    *ethclient.Client // 以太坊RPC连接
	alipayClient *alipay.Client

	fileStorage *fileStorage.FileStorage // 确权合约
	sale        *sale.Sale               // 定价交易合约
	auction     *auction.Auction         // 拍卖交易合约
	license     *license.License         // 授权交易合约
	exchange    *exchange.Exchange       // 交易充值提现合约

	db     *gorm.DB       // 数据库实例
	config *config.Config // 配置文件
}

func New(cfg *config.Config, db *gorm.DB) (*Client, error) {
	// 支付宝
	alipayClient, err := alipay.New(cfg.Alipay.AppId, cfg.Alipay.PrivateKey, false)
	if err != nil {
		return nil, err
	}
	err = alipayClient.LoadAliPayRootCertFromFile(cfg.Alipay.AliPayRootCert)
	if err != nil {
		return nil, err
	}
	err = alipayClient.LoadAliPayPublicCertFromFile(cfg.Alipay.AliPayPublicCert)
	if err != nil {
		return nil, err
	}
	err = alipayClient.LoadAppPublicCertFromFile(cfg.Alipay.AppPublicCert)
	if err != nil {
		return nil, err
	}

	// 连接区块链节点
	client, err := rpc.Dial(cfg.Ethereum.Client.RPCAddress)
	if err != nil {
		return nil, err
	}
	// 创建合约实例
	privateKey, err := crypto.HexToECDSA(cfg.Ethereum.Keystore.PrivateKey)
	if err != nil {
		return nil, err
	}
	ethClient := ethclient.NewClient(client)
	fileStorageContract, err := fileStorage.New(common.HexToAddress(cfg.Ethereum.Client.FileStorageContract), privateKey, ethClient)
	if err != nil {
		return nil, err
	}
	saleContract, err := sale.New(common.HexToAddress(cfg.Ethereum.Client.SaleContract), privateKey, ethClient)
	if err != nil {
		return nil, err
	}
	auctionContract, err := auction.New(common.HexToAddress(cfg.Ethereum.Client.AuctionContract), privateKey, ethClient)
	if err != nil {
		return nil, err
	}
	licenseContract, err := license.New(common.HexToAddress(cfg.Ethereum.Client.LicenseContract), privateKey, ethClient)
	if err != nil {
		return nil, err
	}
	exchangeContract, err := exchange.NewExchange(common.HexToAddress(cfg.Ethereum.Client.ExchangeContract), privateKey, ethClient)
	if err != nil {
		return nil, err
	}
	c := &Client{ethClient: ethClient, alipayClient: alipayClient, fileStorage: fileStorageContract, sale: saleContract, auction: auctionContract, license: licenseContract, exchange: exchangeContract, db: db, config: cfg}

	return c, nil
}

//func (c *Client) generateCertificate(item models.Item, trade models.Trade) error {
//	wd, _ := os.Getwd()
//	im, err := gg.LoadImage(wd + "/test/certificate.png")
//	if err != nil {
//		return err
//	}
//	w := im.Bounds().Size().X
//	h := im.Bounds().Size().Y
//
//	dc := gg.NewContext(w, h)
//	dc.DrawImage(im, 0, 0)
//	dc.SetRGB(0, 0, 0)
//	if err := dc.LoadFontFace(wd+"/test/方正粗黑宋简体.ttf", 50); err != nil {
//		return err
//	}
//	dc.DrawStringAnchored("数字内容存证证明", float64(w/2), 200, 0.5, 0.5)
//
//	if err := dc.LoadFontFace(wd+"/test/方正粗黑宋简体.ttf", 15); err != nil {
//		return err
//	}
//
//	boldText := []string{
//		//fmt.Sprintf("编号：BC%s",item.Hash),
//		"存证用户",
//		"文件哈希值",
//		"区块链交易编号",
//		"数据名称",
//		"数据大小",
//	}
//	//dc.DrawStringAnchored(boldText[0], float64(w/2), 250, 0.5, 0.5)
//	for i := 0; i < len(boldText); i++ {
//		dc.DrawStringAnchored(boldText[i], 130, float64(400+50*i), 0.5, 0.5)
//	}
//
//	if err := dc.LoadFontFace(wd+"/test/方正粗黑宋简体.ttf", 15); err != nil {
//		return err
//	}
//	normalText := []string{
//		//item.Creator.Username,
//		item.FileHash,
//		item.Trades[0].TxHash,
//		item.FileName,
//		strconv.FormatUint(item.FileSize, 10),
//	}
//
//	for i := 0; i < len(boldText); i++ {
//		dc.DrawStringAnchored(normalText[i], 480, float64(400+50*i), 0.5, 0.5)
//	}
//
//	text := fmt.Sprintf("%s接收到安权链区块存证信息，该存证事件包括以下信息，特此证明", time.Unix(int64(trade.BlockTime), 0).Format("2006年01月02日15时04分05秒"))
//	dc.DrawStringWrapped(text, 200, 320, 0.5, 0.5, 200, 1.5, gg.AlignLeft)
//
//	dir := filepath.Join(wd, c.config.TempDir, config.CertificationDir)
//	path := fmt.Sprintf("%s.png", filepath.Join(dir, item.FileHash))
//	err = dc.SavePNG(path)
//	if err != nil {
//		return err
//	}
//
//	return nil
//}
