package client

import (
	"context"
	"gitee.com/aqchain/backend/models"
	"gitee.com/aqchain/go-ethereum/accounts/abi/bind"
	ac "gitee.com/aqchain/go-ethereum/contracts/auction/contract"
	ec "gitee.com/aqchain/go-ethereum/contracts/exchange/contract"
	fsc "gitee.com/aqchain/go-ethereum/contracts/fileStorage/contract"
	sc "gitee.com/aqchain/go-ethereum/contracts/sale/contract"
	"gitee.com/aqchain/go-ethereum/core/types"
	"github.com/smartwalle/alipay/v3"
	"math/big"
	"strconv"
	"time"
)

// 订阅合约事件 使用统一的格式
// func (c *Client) Subscribe[合约名称]Events(ctx context.Context) error {
// 1.初始化事件的channel
// 2.订阅事件
// 3.循环保持协程 接收新事件消息
// }

// SubscribeNewHead 订阅新区块事件
func (c *Client) SubscribeNewHead(ctx context.Context) error {

	headerChan := make(chan *types.Header)
	headerSub, err := c.ethClient.SubscribeNewHead(ctx, headerChan)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case err = <-headerSub.Err():
			return err
		case header := <-headerChan:
			err = c.handleNewHeader(ctx, header)
			if err != nil {
				//return err
				continue
			}
		}
	}
}

func (c *Client) handleNewHeader(ctx context.Context, header *types.Header) error {
	block, err := c.ethClient.BlockByHash(ctx, header.Hash())
	if err != nil {
		return err
	}
	// 保存区块信息
	blockStorage := models.Block{
		Number:       block.NumberU64(),
		Time:         block.Time().Uint64(),
		Hash:         block.Hash().Hex(),
		TxHash:       block.TxHash().Hex(),
		Coinbase:     block.Coinbase().Hex(),
		Size:         float64(block.Size()),
		Transactions: []models.Transaction{},
	}
	// 关联交易信息
	for _, tx := range block.Transactions() {
		sender, err := c.ethClient.TransactionSender(ctx, tx, block.Hash(), uint(block.NumberU64()))
		if err != nil {
			return err
		}
		txStorage := models.Transaction{
			From:        sender.Hex(),
			Hash:        tx.Hash().Hex(),
			BlockNumber: block.NumberU64(),
			Size:        float64(tx.Size()),
			Cost:        tx.Cost().Uint64(),
		}
		if tx.To() != nil {
			txStorage.To = tx.To().Hex()
		}
		blockStorage.Transactions = append(blockStorage.Transactions, txStorage)
	}

	// 因为在其他model中存在Block关联 可能提前创建Block 所以使用Save进行保存
	err = c.db.Save(&blockStorage).Error
	if err != nil {
		return err
	}
	return nil
}

// SubscribeFileStorageEvents 订阅文件存储合约事件
func (c *Client) SubscribeFileStorageEvents(ctx context.Context) error {
	createChan := make(chan *fsc.FileStorageCreate)
	approvalChan := make(chan *fsc.FileStorageApproval)

	createSub, err := c.fileStorage.Contract().WatchCreate(&bind.WatchOpts{Context: ctx}, createChan, nil, nil)
	if err != nil {
		return err
	}
	approvalSub, err := c.fileStorage.Contract().WatchApproval(&bind.WatchOpts{Context: ctx}, approvalChan, nil, nil, nil)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case err = <-createSub.Err():
			return err
		case err = <-approvalSub.Err():
			return err
		case event := <-createChan:
			err = c.handleCreate(event)
			if err != nil {
				return err
			}
		case event := <-approvalChan:
			err = c.handleApproval(event)
			if err != nil {
				return err
			}
		}
	}
}

func (c *Client) handleCreate(create *fsc.FileStorageCreate) error {
	// 查找作品
	item := models.Item{FileHash: create.FileHash}
	err := c.db.Where(&item).First(&item).Error
	if err != nil {
		return err
	}

	// 创建FileStorage记录并更新作品状态 因为对应的Block记录在区块订阅中可能还未添加 所以此处也会关联创建BLock
	fileStorage := models.FileStorage{
		TxHash:   create.Raw.TxHash.String(),
		FileHash: create.FileHash,
		TokenID:  create.TokenId.Uint64(),
		From:     create.Creator.String(),
		Type:     "create",
		Block:    models.Block{Number: create.Raw.BlockNumber},
	}
	err = c.db.Updates(&models.Item{ID: item.ID, Status: 1, FileStorage: fileStorage}).Error
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) handleApproval(approval *fsc.FileStorageApproval) error {

	// 查询第一个创建记录
	created := models.FileStorage{TokenID: approval.TokenId.Uint64(), Type: "create"}
	err := c.db.Where(&created).First(&created).Error
	if err != nil {
		return err
	}

	// 查询作品
	item := models.Item{FileHash: created.FileHash}
	err = c.db.Where(&item).First(&item).Error
	if err != nil {
		return err
	}

	// 创建FileStorage记录并更新作品状态
	fileStorage := models.FileStorage{
		TxHash:   approval.Raw.TxHash.String(),
		FileHash: item.FileHash,
		TokenID:  approval.TokenId.Uint64(),
		From:     approval.Owner.String(),
		To:       approval.Approved.String(),
		Type:     "approval",
		Block:    models.Block{Number: approval.Raw.BlockNumber},
	}

	err = c.db.Create(&fileStorage).Error
	if err != nil {
		return err
	}

	err = c.db.Updates(&models.Item{ID: item.ID, ApprovalTxHash: approval.Raw.TxHash.String()}).Error
	if err != nil {
		return err
	}

	return nil
}

// SubscribeSaleEvents 订阅定价交易合约事件
func (c *Client) SubscribeSaleEvents(ctx context.Context) error {

	createChan := make(chan *sc.SaleCreate)
	cancelChan := make(chan *sc.SaleCancel)
	confirmChan := make(chan *sc.SaleConfirm)

	createSub, err := c.sale.Contract().WatchCreate(&bind.WatchOpts{Context: ctx}, createChan, nil)
	if err != nil {
		return err
	}
	cancelSub, err := c.sale.Contract().WatchCancel(&bind.WatchOpts{Context: ctx}, cancelChan, nil)
	if err != nil {
		return err
	}
	confirmSub, err := c.sale.Contract().WatchConfirm(&bind.WatchOpts{Context: ctx}, confirmChan, nil)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case err = <-createSub.Err():
			return err
		case err = <-cancelSub.Err():
			return err
		case err = <-confirmSub.Err():
			return err
		case event := <-createChan:
			err = c.handleCreateSale(event)
			if err != nil {
				return err
			}
		case event := <-cancelChan:
			err = c.handleCancelSale(event)
			if err != nil {
				return err
			}
		case event := <-confirmChan:
			err = c.handleConfirmSale(event)
			if err != nil {
				return err
			}
		}
	}
}

func (c *Client) handleCreateSale(createSale *sc.SaleCreate) error {
	// 查询第一个创建记录
	created := models.FileStorage{TokenID: createSale.TokenId.Uint64(), Type: "create"}
	err := c.db.Where(&created).First(&created).Error
	if err != nil {
		return err
	}

	// 查询作品
	item := models.Item{FileHash: created.FileHash}
	err = c.db.Where(&item).First(&item).Error
	if err != nil {
		return err
	}

	// 创建Sale记录并更新作品状态
	sale := models.Sale{
		FileHash: created.FileHash,
		TokenID:  created.TokenID,
		TxHash:   createSale.Raw.TxHash.String(),
		Seller:   createSale.Seller.String(),
		Buyer:    createSale.Buyer.String(),
		Price:    createSale.Price.Uint64(),
		Type:     "create",
		Block:    models.Block{Number: createSale.Raw.BlockNumber},
	}

	err = c.db.Updates(&models.Item{ID: item.ID, Status: 2, Sale: sale}).Error
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) handleCancelSale(deleteSale *sc.SaleCancel) error {
	// 查询第一个创建记录
	created := models.FileStorage{TokenID: deleteSale.TokenId.Uint64(), Type: "create"}
	err := c.db.Where(&created).First(&created).Error
	if err != nil {
		return err
	}

	// 查询作品
	item := models.Item{FileHash: created.FileHash}
	err = c.db.Where(&item).First(&item).Error
	if err != nil {
		return err
	}

	// 创建Sale记录并更新作品状态
	sale := models.Sale{
		FileHash: created.FileHash,
		TokenID:  created.TokenID,
		TxHash:   deleteSale.Raw.TxHash.String(),
		Seller:   deleteSale.Seller.String(),
		Buyer:    deleteSale.Buyer.String(),
		Price:    deleteSale.Price.Uint64(),
		Type:     "delete",
		Block:    models.Block{Number: deleteSale.Raw.BlockNumber},
	}

	err = c.db.Updates(&models.Item{ID: item.ID, Status: 1, Sale: sale}).Error
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) handleConfirmSale(confirmSale *sc.SaleConfirm) error {
	// 查询第一个创建记录
	created := models.FileStorage{TokenID: confirmSale.TokenId.Uint64(), Type: "create"}
	err := c.db.Where(&created).First(&created).Error
	if err != nil {
		return err
	}

	// 查询作品
	item := models.Item{FileHash: created.FileHash}
	err = c.db.Where(&item).First(&item).Error
	if err != nil {
		return err
	}

	// 创建Sale记录并更新作品状态
	sale := models.Sale{
		FileHash: created.FileHash,
		TokenID:  created.TokenID,
		TxHash:   confirmSale.Raw.TxHash.String(),
		Seller:   confirmSale.Seller.String(),
		Buyer:    confirmSale.Buyer.String(),
		Price:    confirmSale.Price.Uint64(),
		Type:     "confirm",
		Block:    models.Block{Number: confirmSale.Raw.BlockNumber},
	}

	err = c.db.Updates(&models.Item{ID: item.ID, OwnerAddress: confirmSale.Buyer.String(), Status: 1, Sale: sale}).Error
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) SubscribeAuctionEvent(ctx context.Context) error {

	createChan := make(chan *ac.AuctionCreate)
	bidChan := make(chan *ac.AuctionBid)
	finishChan := make(chan *ac.AuctionFinish)

	createSub, err := c.auction.Contract().WatchCreate(&bind.WatchOpts{Context: ctx}, createChan, nil)
	if err != nil {
		return err
	}
	bidSub, err := c.auction.Contract().WatchBid(&bind.WatchOpts{Context: ctx}, bidChan, nil)
	if err != nil {
		return err
	}
	finishSub, err := c.auction.Contract().WatchFinish(&bind.WatchOpts{Context: ctx}, finishChan, nil)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case err = <-createSub.Err():
			return err
		case err = <-bidSub.Err():
			return err
		case err = <-finishSub.Err():
			return err
		case event := <-createChan:
			err = c.handleCreateAuction(event)
			if err != nil {
				return err
			}
		case event := <-bidChan:
			err = c.handleBidAuction(event)
			if err != nil {
				return err
			}
		case event := <-finishChan:
			err = c.handleFinishAuction(event)
			if err != nil {
				return err
			}
		}
	}
}

func (c *Client) handleCreateAuction(create *ac.AuctionCreate) error {

	// 查询创建token的记录
	created := models.FileStorage{
		TokenID: create.TokenId.Uint64(),
	}
	err := c.db.Where(&created).First(&created).Error
	if err != nil {
		return err
	}

	// 查询作品
	item := &models.Item{FileHash: created.FileHash}
	err = c.db.Where(&item).First(&item).Error
	if err != nil {
		return err
	}

	a := big.NewInt(1000000000)
	b := create.Price
	price1 := big.NewInt(0)
	price := price1.Mul(b, a)
	price2 := price.Uint64()

	// 创建交易创建记录并更新作品状态
	newAuction := models.Auction{
		FileHash: created.FileHash,
		TxHash:   create.Raw.TxHash.String(),
		TokenID:  created.TokenID,
		Seller:   create.Seller.String(),
		//创建时输入的是以太币，需要转化为gei为单位存到数据库中
		Price:        price2,
		Begin:        create.Begin.Uint64(),
		End:          create.End.Uint64(),
		Type:         "create",
		CreateTxHash: create.Raw.TxHash.String(),
		Block:        models.Block{Number: create.Raw.BlockNumber},
	}

	err = c.db.Updates(&models.Item{ID: item.ID, Status: 3, Auction: newAuction}).Error
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) handleBidAuction(bid *ac.AuctionBid) error {

	// 查询创建token的记录
	created := models.FileStorage{
		TokenID: bid.TokenId.Uint64(),
	}
	err := c.db.Where(&created).First(&created).Error
	if err != nil {
		return err
	}
	// 查询作品
	item := &models.Item{FileHash: created.FileHash}
	err = c.db.Preload("Auction").Where(&item).First(&item).Error
	if err != nil {
		return err
	}
	auction := item.Auction

	//b:=bid.Bid.Uint64()
	//fmt.Println(b)
	a := big.NewInt(1000000000)
	b := bid.Bid

	price1 := big.NewInt(0)
	price1.Quo(b, a)
	price := price1.Uint64()

	// 创建交易创建记录并更新作品状态
	newAuction := models.Auction{
		TxHash:   bid.Raw.TxHash.String(),
		FileHash: item.FileHash,
		TokenID:  bid.TokenId.Uint64(),
		Seller:   auction.Seller,
		Bidder:   bid.Bider.String(),
		//关于出价的价格，数据库存储的是gwei，但是接收到的是以wei为单位的
		Price:        price,
		Begin:        auction.Begin,
		End:          auction.End,
		Type:         "bid",
		CreateTxHash: auction.CreateTxHash,
		Block:        models.Block{Number: bid.Raw.BlockNumber},
	}

	err = c.db.Create(&newAuction).Error
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) handleFinishAuction(finish *ac.AuctionFinish) error {
	// 查询创建token的记录
	created := models.FileStorage{
		TokenID: finish.TokenId.Uint64(),
	}
	err := c.db.Where(&created).First(&created).Error
	if err != nil {
		return err
	}
	// 查询作品
	item := &models.Item{FileHash: created.FileHash}
	err = c.db.Preload("Auction").Where(&item).First(&item).Error
	if err != nil {
		return err
	}

	auction := item.Auction
	// 查询创建交易记录
	err = c.db.Where(&auction).First(&auction).Error
	if err != nil {
		return err
	}

	// 创建交易创建记录
	newAuction := models.Auction{
		TxHash:       finish.Raw.TxHash.String(),
		FileHash:     item.FileHash,
		TokenID:      finish.TokenId.Uint64(),
		Seller:       auction.Seller,
		Bidder:       finish.HighestBidder.String(),
		Price:        finish.HighestBid.Uint64(),
		Begin:        auction.Begin,
		End:          auction.End,
		Type:         "finish",
		CreateTxHash: auction.CreateTxHash,
		BlockNumber:  finish.Raw.BlockNumber,
	}

	// 更新作品状态
	err = c.db.Select("Status", "OwnerAddress", "ApprovalTxHash", "AuctionTxHash").Updates(&models.Item{ID: item.ID, Status: 1, OwnerAddress: newAuction.Bidder, ApprovalTxHash: "", Auction: newAuction}).Error
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) SubscribeExchangeEvent(ctx context.Context) error {
	c2eChan := make(chan *ec.ExchangeCurrencyToEth)
	e2cChan := make(chan *ec.ExchangeEthToCurrency)

	c2eSub, err := c.exchange.Contract().WatchCurrencyToEth(&bind.WatchOpts{Context: ctx}, c2eChan, nil)
	if err != nil {
		return err
	}
	e2cSub, err := c.exchange.Contract().WatchEthToCurrency(&bind.WatchOpts{Context: ctx}, e2cChan, nil)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case err = <-c2eSub.Err():
			return err
		case err = <-e2cSub.Err():
			return err
		case event := <-c2eChan:
			err = c.handleCurrencyToEth(event)
			if err != nil {
				return err
			}
		case event := <-e2cChan:
			err = c.handleEthToCurrency(event)
			if err != nil {
				return err
			}
		}
	}
}

func (c *Client) handleCurrencyToEth(c2e *ec.ExchangeCurrencyToEth) error {

	// 根据TxHash查找Order
	deposit := models.Deposit{OrderNo: c2e.OrderNo}
	err := c.db.Where(&deposit).First(&deposit).Error
	if err != nil {
		return err
	}
	// todo 检查内容？

	// 更新Order
	err = c.db.Model(&deposit).Updates(&models.Deposit{Status: 2}).Error
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) handleEthToCurrency(e2c *ec.ExchangeEthToCurrency) error {
	// 根据TxHash查找Order
	withdraw := models.Withdraw{OrderNo: e2c.OrderNo}
	err := c.db.Preload("Account").Where(&withdraw).First(&withdraw).Error
	if err != nil {
		return err
	}

	// todo 检查内容？

	// 支付宝转账
	var param = alipay.FundTransUniTransfer{
		OutBizNo:    string(rune(time.Now().Second())),
		TransAmount: strconv.FormatFloat(withdraw.Amount, 'f', 2, 64),
		ProductCode: "TRANS_ACCOUNT_NO_PWD",
		BizScene:    "DIRECT_TRANSFER",
		PayeeInfo: &alipay.PayeeInfo{
			Identity:     withdraw.Account.AlipayID,
			IdentityType: "ALIPAY_LOGON_ID",
			Name:         withdraw.Account.AlipayName,
		},
	}
	rsp, err := c.alipayClient.FundTransUniTransfer(param)
	if err != nil {
		return err
	}
	if rsp.IsSuccess() {
		// 更新Order
		err = c.db.Model(&withdraw).Updates(&models.Withdraw{Status: 2, TxHash: e2c.Raw.TxHash.String()}).Error
		if err != nil {
			return err
		}
	}
	return nil
}
