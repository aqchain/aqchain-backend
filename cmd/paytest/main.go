package main

import (
	"gitee.com/aqchain/backend/config"
	"gitee.com/aqchain/backend/utils"
	"github.com/smartwalle/alipay/v3"
	"log"
	"os"
	"time"
)

func main() {

	wd, _ := os.Getwd()
	// 获取配置
	cfg, err := config.New(wd, "config.json")
	utils.Fatal(err)

	client, err := alipay.New(cfg.Alipay.AppId, cfg.Alipay.PrivateKey, false)
	utils.Fatal(err)

	err = client.LoadAliPayRootCertFromFile(cfg.Alipay.AliPayRootCert)
	utils.Fatal(err)
	err = client.LoadAliPayPublicCertFromFile(cfg.Alipay.AliPayPublicCert)
	utils.Fatal(err)
	err = client.LoadAppPublicCertFromFile(cfg.Alipay.AppPublicCert)
	utils.Fatal(err)

	var param = alipay.FundTransUniTransfer{
		OutBizNo:    time.Now().Format("20060102150405"),
		TransAmount: "1.00",
		ProductCode: "TRANS_ACCOUNT_NO_PWD",
		BizScene:    "DIRECT_TRANSFER",
		PayeeInfo: &alipay.PayeeInfo{
			Identity:     "axmyor6190@sandbox.com",
			IdentityType: "ALIPAY_LOGON_ID",
			Name:         "axmyor6190",
		},
	}
	rsp, err := client.FundTransUniTransfer(param)
	utils.Fatal(err)
	log.Println(rsp.Content)

	var param2 = alipay.TradePagePay{
		Trade: alipay.Trade{
			NotifyURL:   "http://d7f42c25f7b6.ngrok.io/api/alipayNotify",
			ReturnURL:   "http://d7f42c25f7b6.ngrok.io/api/alipayReturn",
			Subject:     "test",
			OutTradeNo:  time.Now().Format("20060102150405"),
			TotalAmount: "1.00",
			ProductCode: "FAST_INSTANT_TRADE_PAY",
		},
	}
	url, err := client.TradePagePay(param2)
	utils.Fatal(err)
	log.Println(url.String())

	//var privateKey = "MIIEpQIBAAKCAQEAjLJouC8UGQN3d+VbrXqSTO9wJ05rt0I3Q1YyCsc/kWENqkFttpiUh3+G32IGCeNnHRpSmXF4eTkASwxBSaWZ+ERBXxKuLfolKSW2HDwkNah24RSUcHJvumRibDTfSh2z1+NhUw2/KqNv9io/jAYCPp9jcquunqhnDo28bbLnfa+4kIrLNIy/XoliELHBipPHu2oKXa8t7OtM8DsGsw6pTsfiLm7aoUwc9RTgDfH0AbLWD30sN6gFZ02t2leQp4B4Q3OfymbLkM2scb4raIuYr8j7Dy5c98+znna8d0H3iikQJz8psSX8wjbBkbUI8Fn1/I6v9UxgLllAAe7mfjXG5QIDAQABAoIBAQCC5y81RmANkcMioxu0ARmDSTbw6y6e95WRKe4vRwhmy4S6Rlu8iqOOmeEzjvENXC1Az4/QQdmT96vFJ2bLDa9+ZIrp2OiD0Huol/SiunX6En5glhQxttcq9WD5SgwB2jbMgzq22lBlbGCsCMzNC34lI4iUVRWHeilASjDpY42rymOtYodTN1wV08UmLrC6GJc6Kx2EgbilL8XvtkWVS/A88wfhTFzbSwPMGctaLEhEc5nu2nM/QGVmYdTaY7gVpCoKBkl8JxHnbbVd2x21ExPpSfbDwptp2RO2IKZ/V4ICLB7+njbt30WPaWfqxJ8uijldkyDZD+SKtu9K+YIN7o+1AoGBAMS1xLUXJ+8VJR9ePl1b+FVXW41SLPYtpAwBAxcAbPyK96/pL/SiSUcZg9S2df++A65g+nEON1eTcXXpIwisQdybIibUNG7lZCj6OZFMegkA2nhGm3MCH+BwYewO6fYbfswMYSLV0QB2MFoynm45CYo9H6EuF0otjbjCkmgCgS9vAoGBALcapNCgolyMJcEVPaKGjx1aN9x5EfjGb1cA7TlLSOJLpgYTiP3rmRfRpKdAQowI9PpJRaOfWccpct20RECaKba3NXEKpjJfVGI2fHz9CwMCQBLjRSzBSLRjRRS5hDSP7+0GsnUOnopZL/EZwMJPlwW0m7CI8P0neBWREWnIdoTrAoGAdthG5w39b4H78q6yyR7ms1DbHE+4oGdExIfcAQqdzhp7uMkO0yM1WIJ+1oYCVqfLe5BV3vYy03NeSlm3c+b2x7zmbkUpBJMM54D+RN3KoSPl2mL2Sg7PiX78em0sRyenfMpB6ZT69+dwIR6R/4/wRpfVQ0Egiw1ooWbUo4hX2nsCgYEAliCsXbo3JTZrhiJKRMWa9fAQuR4zVAwvIeDKoB1QBusLlxPRUJApF9R18gPIDFUTt1r4VC3FSHx9h7XT+DvuU7dng+W9WrO61kPiUgCPVMo1isb44rcfS3fRHir1cLtLdsX07/EqoDh8DGyXFTp4kt8Af//nx0jjNpuY8WjBdD0CgYEAviPEOIgfEVHL1jCN26/sOp5dyPrPSBVjJknc8OuE6Uw8KWjtPEmGPV2azI3HiXcZfSGKJd/ktP7Q6hONPBAcmoLvs3mUQtyK9yy/ebF5ajSwfN64cNDfxjtA8eB9pK1Y9/zrH9WZ9PTy+x6fwaF9cpUYyvcLWGiGZKw9a8YD8Zs="
	//
	//var appId = "2021000117668121"
	//
	//var aliPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjLJouC8UGQN3d+VbrXqSTO9wJ05rt0I3Q1YyCsc/kWENqkFttpiUh3+G32IGCeNnHRpSmXF4eTkASwxBSaWZ+ERBXxKuLfolKSW2HDwkNah24RSUcHJvumRibDTfSh2z1+NhUw2/KqNv9io/jAYCPp9jcquunqhnDo28bbLnfa+4kIrLNIy/XoliELHBipPHu2oKXa8t7OtM8DsGsw6pTsfiLm7aoUwc9RTgDfH0AbLWD30sN6gFZ02t2leQp4B4Q3OfymbLkM2scb4raIuYr8j7Dy5c98+znna8d0H3iikQJz8psSX8wjbBkbUI8Fn1/I6v9UxgLllAAe7mfjXG5QIDAQAB"
	//
	//client, err := alipay.New(appId, privateKey, false)
	//if err != nil {
	//	panic(err)
	//}
	//
	//err = client.LoadAliPayPublicKey(aliPublicKey)
	//if err != nil {
	//	panic(err)
	//}

	//var p = alipay.TradePagePay{}
	//p.NotifyURL = "http://85.203.23.37:8080/api/alipayReturn"
	////p.ReturnURL = "http://localhost:8080/return"
	//p.Subject = "测试"
	//p.OutTradeNo = time.Now().String()
	//p.TotalAmount = "0.01"
	//p.ProductCode = "FAST_INSTANT_TRADE_PAY"
	//
	//url, err := client.TradePagePay(p)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//payURL := url.String()
	//fmt.Println(payURL)
}
