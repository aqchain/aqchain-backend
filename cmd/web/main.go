package main

import (
	"fmt"
	"gitee.com/aqchain/backend/config"
	"gitee.com/aqchain/backend/controller"
	"gitee.com/aqchain/backend/database"
	"gitee.com/aqchain/backend/utils"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"os"
)

func main() {
	wd, _ := os.Getwd()
	// 获取配置
	cfg, err := config.New(wd, "config_alipay8121.json")
	utils.Fatal(err)

	// 数据库
	db, err := database.Database(cfg.Database)
	utils.Fatal(err)

	// gin配置
	clr, err := controller.New(cfg, db)
	utils.Fatal(err)

	r := gin.Default()

	r.Use(gin.Logger())
	r.Use(cors.Default())

	api := r.Group("/api")

	api.POST("/account", clr.CreateAccount)
	api.GET("/account/:address", clr.GetAccount)
	api.PUT("/account", clr.UpdateAccount)

	api.POST("/item", clr.CreateItem)
	api.GET("/item/:fileHash", clr.GetItem)
	api.PUT("/item", clr.UpdateItem)
	api.GET("/items/", clr.GetItems)

	api.GET("/fileStorages/", clr.GetFileStorages)

	api.GET("/sales/", clr.GetSales)

	api.GET("/auction/:txHash", clr.GetAuction)
	api.GET("/auctions/", clr.GetAuctions)

	api.POST("/deposit", clr.CreateDeposit)
	api.GET("/deposits", clr.GetDeposits)
	api.POST("/alipayNotify", clr.AlipayNotify)
	api.GET("/alipayReturn", clr.AlipayReturn)

	api.POST("/withdraw", clr.CreateWithdraw)
	api.GET("/withdraws", clr.GetWithdraws)

	api.GET("/file/:fileHash", clr.GetFile)

	r.Run(fmt.Sprintf("%s:%s", cfg.Server.Host, cfg.Server.Port))

}
