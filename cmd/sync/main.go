package main

import (
	"context"
	"gitee.com/aqchain/backend/client"
	"gitee.com/aqchain/backend/config"
	"gitee.com/aqchain/backend/database"
	"gitee.com/aqchain/backend/utils"
	"github.com/x-mod/routine"
	"log"
	"os"
	"syscall"
)

func main() {
	wd, err := os.Getwd()
	utils.Fatal(err)

	// 配置文件
	cfg, err := config.New(wd, "config.json")
	utils.Fatal(err)

	// 数据库
	db, err := database.Database(cfg.Database)
	utils.Fatal(err)

	// 以太坊客户端连接
	cli, err := client.New(cfg, db)
	utils.Fatal(err)

	ctx, cancel := context.WithCancel(context.TODO())
	err = routine.Main(
		ctx,
		routine.ExecutorFunc(func(ctx context.Context) error {
			log.Println("main begin")
			defer log.Println("main end")
			select {
			case <-ctx.Done():
				return nil
			}
		}),
		routine.Signal(syscall.SIGINT, func() {
			cancel()
		}),
		routine.Go(routine.ExecutorFunc(cli.SubscribeNewHead)),
		routine.Go(routine.ExecutorFunc(cli.SubscribeFileStorageEvents)),
		routine.Go(routine.ExecutorFunc(cli.SubscribeSaleEvents)),
		routine.Go(routine.ExecutorFunc(cli.SubscribeAuctionEvent)),
		routine.Go(routine.ExecutorFunc(cli.SubscribeExchangeEvent)),
	)
	utils.Fatal(err)
}
