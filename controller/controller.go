package controller

import (
	"gitee.com/aqchain/backend/config"
	"gitee.com/aqchain/backend/utils"
	"gitee.com/aqchain/go-ethereum/common"
	"gitee.com/aqchain/go-ethereum/contracts/exchange"
	"gitee.com/aqchain/go-ethereum/crypto"
	"gitee.com/aqchain/go-ethereum/ethclient"
	"gitee.com/aqchain/go-ethereum/rpc"
	"github.com/smartwalle/alipay/v3"
	"gorm.io/gorm"
)

type Controller struct {
	config       *config.Config
	db           *gorm.DB
	alipayClient *alipay.Client
	ethClient    *ethclient.Client
	exchange     *exchange.Exchange
}

func New(cfg *config.Config, db *gorm.DB) (*Controller, error) {
	// 支付宝
	//var privateKey = "MIIEpQIBAAKCAQEAjLJouC8UGQN3d+VbrXqSTO9wJ05rt0I3Q1YyCsc/kWENqkFttpiUh3+G32IGCeNnHRpSmXF4eTkASwxBSaWZ+ERBXxKuLfolKSW2HDwkNah24RSUcHJvumRibDTfSh2z1+NhUw2/KqNv9io/jAYCPp9jcquunqhnDo28bbLnfa+4kIrLNIy/XoliELHBipPHu2oKXa8t7OtM8DsGsw6pTsfiLm7aoUwc9RTgDfH0AbLWD30sN6gFZ02t2leQp4B4Q3OfymbLkM2scb4raIuYr8j7Dy5c98+znna8d0H3iikQJz8psSX8wjbBkbUI8Fn1/I6v9UxgLllAAe7mfjXG5QIDAQABAoIBAQCC5y81RmANkcMioxu0ARmDSTbw6y6e95WRKe4vRwhmy4S6Rlu8iqOOmeEzjvENXC1Az4/QQdmT96vFJ2bLDa9+ZIrp2OiD0Huol/SiunX6En5glhQxttcq9WD5SgwB2jbMgzq22lBlbGCsCMzNC34lI4iUVRWHeilASjDpY42rymOtYodTN1wV08UmLrC6GJc6Kx2EgbilL8XvtkWVS/A88wfhTFzbSwPMGctaLEhEc5nu2nM/QGVmYdTaY7gVpCoKBkl8JxHnbbVd2x21ExPpSfbDwptp2RO2IKZ/V4ICLB7+njbt30WPaWfqxJ8uijldkyDZD+SKtu9K+YIN7o+1AoGBAMS1xLUXJ+8VJR9ePl1b+FVXW41SLPYtpAwBAxcAbPyK96/pL/SiSUcZg9S2df++A65g+nEON1eTcXXpIwisQdybIibUNG7lZCj6OZFMegkA2nhGm3MCH+BwYewO6fYbfswMYSLV0QB2MFoynm45CYo9H6EuF0otjbjCkmgCgS9vAoGBALcapNCgolyMJcEVPaKGjx1aN9x5EfjGb1cA7TlLSOJLpgYTiP3rmRfRpKdAQowI9PpJRaOfWccpct20RECaKba3NXEKpjJfVGI2fHz9CwMCQBLjRSzBSLRjRRS5hDSP7+0GsnUOnopZL/EZwMJPlwW0m7CI8P0neBWREWnIdoTrAoGAdthG5w39b4H78q6yyR7ms1DbHE+4oGdExIfcAQqdzhp7uMkO0yM1WIJ+1oYCVqfLe5BV3vYy03NeSlm3c+b2x7zmbkUpBJMM54D+RN3KoSPl2mL2Sg7PiX78em0sRyenfMpB6ZT69+dwIR6R/4/wRpfVQ0Egiw1ooWbUo4hX2nsCgYEAliCsXbo3JTZrhiJKRMWa9fAQuR4zVAwvIeDKoB1QBusLlxPRUJApF9R18gPIDFUTt1r4VC3FSHx9h7XT+DvuU7dng+W9WrO61kPiUgCPVMo1isb44rcfS3fRHir1cLtLdsX07/EqoDh8DGyXFTp4kt8Af//nx0jjNpuY8WjBdD0CgYEAviPEOIgfEVHL1jCN26/sOp5dyPrPSBVjJknc8OuE6Uw8KWjtPEmGPV2azI3HiXcZfSGKJd/ktP7Q6hONPBAcmoLvs3mUQtyK9yy/ebF5ajSwfN64cNDfxjtA8eB9pK1Y9/zrH9WZ9PTy+x6fwaF9cpUYyvcLWGiGZKw9a8YD8Zs="
	//
	//var appId = "2021000117668121"
	//
	//var aliPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjLJouC8UGQN3d+VbrXqSTO9wJ05rt0I3Q1YyCsc/kWENqkFttpiUh3+G32IGCeNnHRpSmXF4eTkASwxBSaWZ+ERBXxKuLfolKSW2HDwkNah24RSUcHJvumRibDTfSh2z1+NhUw2/KqNv9io/jAYCPp9jcquunqhnDo28bbLnfa+4kIrLNIy/XoliELHBipPHu2oKXa8t7OtM8DsGsw6pTsfiLm7aoUwc9RTgDfH0AbLWD30sN6gFZ02t2leQp4B4Q3OfymbLkM2scb4raIuYr8j7Dy5c98+znna8d0H3iikQJz8psSX8wjbBkbUI8Fn1/I6v9UxgLllAAe7mfjXG5QIDAQAB"
	//
	//alipayClient, err := alipay.New(appId, privateKey, false)
	//if err != nil {
	//	return nil, err
	//}
	//err = alipayClient.LoadAliPayPublicKey(aliPublicKey)
	alipayClient, err := alipay.New(cfg.Alipay.AppId, cfg.Alipay.PrivateKey, false)
	if err != nil {
		return nil, err
	}
	err = alipayClient.LoadAliPayRootCertFromFile(cfg.Alipay.AliPayRootCert)
	if err != nil {
		return nil, err
	}
	err = alipayClient.LoadAliPayPublicCertFromFile(cfg.Alipay.AliPayPublicCert)
	if err != nil {
		return nil, err
	}
	err = alipayClient.LoadAppPublicCertFromFile(cfg.Alipay.AppPublicCert)
	if err != nil {
		return nil, err
	}

	// 区块链
	client, err := rpc.Dial(cfg.Ethereum.Client.RPCAddress)
	if err != nil {
		return nil, err
	}
	ethClient := ethclient.NewClient(client)
	privateKey, err := crypto.HexToECDSA(cfg.Ethereum.Keystore.PrivateKey)
	if err != nil {
		return nil, err
	}
	newExchange, err := exchange.NewExchange(common.HexToAddress(cfg.Ethereum.Client.ExchangeContract), privateKey, ethClient)
	if err != nil {
		return nil, err
	}

	return &Controller{config: cfg, db: db, alipayClient: alipayClient, ethClient: ethClient, exchange: newExchange}, nil
}

func verifySignature(address, signature, message string) error {
	if !common.IsHexAddress(address) {
		return errAddress
	}
	recoveredAddress, err := utils.EcRecover(message, signature)
	if err != nil {
		return err
	}
	if !utils.IsSameAddress(recoveredAddress.String(), address) {
		return errSignature
	}
	return nil
}
