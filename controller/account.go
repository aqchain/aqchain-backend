package controller

import (
	"gitee.com/aqchain/backend/models"
	"gitee.com/aqchain/backend/utils"
	"gitee.com/aqchain/go-ethereum/common"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"gorm.io/gorm"
	"log"
	"net/http"
)

// CreateAccount 创建钱包账户信息
func (c *Controller) CreateAccount(ctx *gin.Context) {
	// 获取参数
	accountReq := models.AccountPost{}
	err := ctx.ShouldBindWith(&accountReq, binding.JSON)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errParams.Error(),
		})
		return
	}

	// 检查参数
	if !common.IsHexAddress(accountReq.Address) {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errAddress.Error(),
		})
		return
	}
	recoveredAddress, err := utils.EcRecover(models.AccountMessage, accountReq.Signature)
	if err != nil || !utils.IsSameAddress(recoveredAddress.String(), accountReq.Address) {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errSignature.Error(),
		})
		return
	}

	// 数据库操作
	account := models.Account{
		Address:  accountReq.Address,
		Mail:     accountReq.Mail,
		Username: accountReq.Username,
	}
	err = c.db.Create(&account).Error
	if err != nil {
		log.Println(err)
		ctx.JSON(http.StatusInternalServerError, models.Response{
			Success:      false,
			ErrorMessage: errDBError.Error(),
		})
		return
	}

	// 返回结果
	ctx.JSON(http.StatusOK, models.Response{
		Success: true,
		Data:    account,
	})
}

// GetAccount 获取钱包账户信息
func (c *Controller) GetAccount(ctx *gin.Context) {
	// 获取参数
	accountGet := models.AccountGet{}
	err := ctx.BindUri(&accountGet)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	// 检查参数
	if "" == accountGet.Address || !common.IsHexAddress(accountGet.Address) {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errAddress.Error()))
		return
	}
	// 数据库操作
	account := models.Account{Address: accountGet.Address}
	err = c.db.Where(&account).First(&account).Error
	if err != nil {
		if err != gorm.ErrRecordNotFound {
			ctx.JSON(http.StatusInternalServerError, models.NewErrorResponse(errDBError.Error()))
			return
		} else {
			// 返回结果
			ctx.JSON(http.StatusOK, models.NewErrorResponse(errAccountNotFound.Error()))
			return
		}
	}

	// 返回结果
	ctx.JSON(http.StatusOK, models.NewSuccessResponse(account))
}

func (c *Controller) UpdateAccount(ctx *gin.Context) {
	// 获取参数
	accountReq := models.AccountPost{}
	err := ctx.MustBindWith(&accountReq, binding.JSON)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errParams.Error(),
		})
		return
	}

	// 检查签名和地址
	recoveredAddress, err := utils.EcRecover(models.AccountMessage, accountReq.Signature)
	if err != nil || !utils.IsSameAddress(recoveredAddress.String(), accountReq.Address) {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errSignature.Error(),
		})
		return
	}

	// 数据库操作
	account := models.Account{
		Mail:       accountReq.Mail,
		Username:   accountReq.Username,
		AlipayID:   accountReq.AlipayID,
		AlipayName: accountReq.AlipayName,
	}
	err = c.db.Model(&models.Account{}).Where(&models.Account{Address: accountReq.Address}).Updates(&account).Error
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.Response{
			Success:      false,
			ErrorMessage: errDBError.Error(),
		})
		return
	}

	// 返回结果
	ctx.JSON(http.StatusOK, models.Response{
		Success: true,
		Data:    account,
	})
}
