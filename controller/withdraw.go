package controller

import (
	"gitee.com/aqchain/backend/models"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"net/http"
)

func (c *Controller) CreateWithdraw(ctx *gin.Context) {
	// 获取参数
	post := models.WithdrawPost{}
	err := ctx.ShouldBindWith(&post, binding.JSON)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errParams.Error(),
		})
		return
	}

	// 检查签名
	err = verifySignature(post.Address, post.Signature, models.WithdrawSignMessage)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: err.Error(),
		})
		return
	}

	// 创建提现记录
	withdraw := models.Withdraw{
		Amount:         post.Amount,
		AccountAddress: post.Address,
		Type:           post.Type,
	}

	err = c.db.Create(&withdraw).Error
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.Response{
			Success:      false,
			ErrorMessage: errDBError.Error(),
		})
		return
	}

	// 返回结果
	ctx.JSON(http.StatusOK, models.Response{
		Success: true,
		Data:    withdraw,
	})
}

func (c *Controller) GetWithdraws(ctx *gin.Context) {
	query := models.WithdrawQuery{}
	err := ctx.ShouldBindWith(&query, binding.Query)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	// 条件查询
	tx := c.db.Preload("Account").
		Limit(int(query.PageSize)).Offset(int((query.Current - 1) * query.PageSize)).
		Where(&models.Withdraw{AccountAddress: query.AccountAddress})

	var records []models.Withdraw
	err = tx.Find(&records).Error
	if err != nil {
		ctx.JSON(200, models.NewErrorCollectionResponse(err.Error()))
		return
	}

	ctx.JSON(200, models.NewSuccessCollectionResponse(records))
}
