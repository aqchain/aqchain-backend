package controller

import (
	"encoding/binary"
	"gitee.com/aqchain/backend/config"
	"gitee.com/aqchain/backend/models"
	"gitee.com/aqchain/backend/utils"
	"gitee.com/aqchain/go-ethereum/common"
	"gitee.com/aqchain/go-ethereum/crypto"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"gorm.io/gorm"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

// CreateItem 创建作品信息并保存上传文件
func (c *Controller) CreateItem(ctx *gin.Context) {
	// 获取请求参数
	post := models.ItemPost{}
	err := ctx.ShouldBindWith(&post, binding.Form)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errPostRequestParamsLacking.Error(),
		})
		return
	}

	post.File, err = ctx.FormFile("file")
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errPostRequestFileLacking.Error(),
		})
		return
	}

	// 检查签名
	err = verifySignature(post.Address, post.Signature, models.ItemSignMessage)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: err.Error(),
		})
		return
	}

	// 检查账户是否存在
	account := models.Account{Address: post.Address}
	err = c.db.Where(&account).First(&account).Error
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errAccountNotFound.Error(),
		})
		return
	}

	// 生成文件Hash
	fileHash, _, err := getFileHash(post.File)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.Response{
			Success:      false,
			ErrorMessage: err.Error(),
		})
		return
	}

	// 检查文件是否已存在
	item := models.Item{FileHash: fileHash.String()}
	c.db.Where(&item).First(&item)
	if item.ID > 0 {
		// 文件已存在且上传者相同
		if item.OwnerAddress == account.Address {
			// 查询文件状态是否已经上链
			if item.Status > 0 {
				ctx.JSON(http.StatusBadRequest, models.Response{
					Success:      false,
					ErrorMessage: "文件已确权",
				})
				return
			} else {
				// 返回结果
				ctx.JSON(http.StatusOK, models.Response{
					Success: true,
					Data:    item,
				})
				return
			}
		} else {
			ctx.JSON(http.StatusBadRequest, models.Response{
				Success:      false,
				ErrorMessage: errFileExist.Error(),
			})
			return
		}
	}

	// 文件上传保存
	wd, _ := os.Getwd()
	path := filepath.Join(wd, "temp", config.ItemDir, fileHash.String())
	err = ctx.SaveUploadedFile(post.File, path)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.Response{
			Success:      false,
			ErrorMessage: err.Error(),
		})
		return
	}

	// 数据库操作
	item = models.Item{
		Name:           post.Name,
		Description:    post.Description,
		Category:       post.Category,
		FileName:       post.FileName,
		FileSize:       post.FileSize,
		FileType:       post.FileType,
		FileHash:       fileHash.String(),
		CreatorAddress: account.Address,
		OwnerAddress:   account.Address,
	}
	err = c.db.Omit("FileStorageTxHash", "SaleTxHash", "AuctionTxHash").Create(&item).Error
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.Response{
			Success:      false,
			ErrorMessage: err.Error(),
		})
		return
	}

	// 返回结果
	ctx.JSON(http.StatusOK, models.Response{
		Success: true,
		Data:    item,
	})
}

func (c *Controller) GetItem(ctx *gin.Context) {
	// 获取参数
	fileHash := ctx.Param("fileHash")
	if fileHash == "" {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	item := models.Item{FileHash: fileHash}

	err := c.db.Preload("Creator").Preload("Owner").Preload("FileStorage").Preload("FileStorage.Block").
		Preload("Sale").Preload("Sale.Block").Preload("Auction").Preload("Auction.Block").
		Where(&item).First(&item).Error

	if err != nil {
		if err != gorm.ErrRecordNotFound {
			ctx.JSON(http.StatusInternalServerError, models.NewErrorResponse(errDBError.Error()))
			return
		} else {
			// 返回结果
			ctx.JSON(http.StatusOK, models.NewErrorResponse(errAccountNotFound.Error()))
			return
		}
	}
	// 返回结果
	ctx.JSON(http.StatusOK, models.NewSuccessResponse(item))
}

func (c *Controller) UpdateItem(ctx *gin.Context) {
	// 获取参数
	req := models.ItemPut{}
	err := ctx.MustBindWith(&req, binding.JSON)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	// 检查签名和地址是否对应
	recoveredAddr, err := utils.EcRecover(models.ItemSignMessage, req.Signature)
	if err != nil || !utils.IsSameAddress(req.Address, recoveredAddr.String()) {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errSignature.Error()))
		return
	}

	// 检查文件是否存在
	item := models.Item{FileHash: req.FileHash}
	err = c.db.Where(&item).First(&item).Error
	if err != nil {
		if err != gorm.ErrRecordNotFound {
			ctx.JSON(http.StatusInternalServerError, models.NewErrorResponse(errDBError.Error()))
			return
		}
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errFileNotExist.Error()))
		return
	}

	// 检查作品拥有者和请求地址是否对应
	if item.OwnerAddress != req.Address {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errNotOwner.Error()))
		return
	}

	// 数据更新
	item = models.Item{
		Name:        req.Name,
		Description: req.Description,
	}
	err = c.db.Model(&models.Item{}).Where(&models.Item{FileHash: req.FileHash}).Updates(&item).Error
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.NewErrorResponse(errDBError.Error()))
		return
	}

	ctx.JSON(200, models.NewSuccessResponse(item))
}

func (c *Controller) GetItems(ctx *gin.Context) {
	itemQuery := models.ItemsQuery{}
	err := ctx.MustBindWith(&itemQuery, binding.Query)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	// 条件查询
	tx := c.db.Preload("Creator").Preload("Owner").
		Limit(int(itemQuery.PageSize)).Offset(int((itemQuery.Current - 1) * itemQuery.PageSize)).
		Where(&models.Item{CreatorAddress: itemQuery.CreatorAddress, OwnerAddress: itemQuery.OwnerAddress, Status: itemQuery.Status, Name: itemQuery.Name, Category: itemQuery.Category})

	var items []models.Item
	err = tx.Find(&items).Error
	if err != nil {
		ctx.JSON(200, models.NewErrorCollectionResponse(err.Error()))
		return
	}

	ctx.JSON(200, models.NewSuccessCollectionResponse(items))
}

func getFileHash(file *multipart.FileHeader) (common.Hash, common.Hash, error) {
	src, err := file.Open()
	if err != nil {
		return common.Hash{}, common.Hash{}, err
	}
	fileBytes, err := ioutil.ReadAll(src)
	if err != nil {
		return common.Hash{}, common.Hash{}, err
	}
	fileHash := crypto.Keccak256Hash(fileBytes)

	fileHashBytes := fileHash.Bytes()
	binary.BigEndian.PutUint64(fileHashBytes, uint64(time.Now().Unix()))
	fileAndTimeHash := crypto.Keccak256Hash(fileHashBytes)
	return fileHash, fileAndTimeHash, nil
}

func (c *Controller) GetFile(ctx *gin.Context) {
	// 获取参数
	fileHash := ctx.Param("fileHash")
	if fileHash == "" {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	wd, _ := os.Getwd()
	path := filepath.Join(wd, "temp", config.ItemDir, fileHash)
	ctx.File(path)
}
