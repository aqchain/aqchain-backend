package controller

import (
	"gitee.com/aqchain/backend/models"
	"gitee.com/aqchain/backend/utils"
	"gitee.com/aqchain/go-ethereum/common"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/smartwalle/alipay/v3"
	"net/http"
	"strconv"
)

// CreateDeposit 创建充值记录
func (c *Controller) CreateDeposit(ctx *gin.Context) {
	// 获取参数
	orderPost := models.DepositPost{}
	err := ctx.ShouldBindWith(&orderPost, binding.JSON)

	// 检查参数
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errParams.Error(),
		})
		return
	}

	// 检查参数
	if !common.IsHexAddress(orderPost.Address) {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errAddress.Error(),
		})
		return
	}
	recoveredAddress, err := utils.EcRecover(models.DepositSignMessage, orderPost.Signature)
	if err != nil || !utils.IsSameAddress(recoveredAddress.String(), orderPost.Address) {
		ctx.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: errSignature.Error(),
		})
		return
	}
	deposit := models.Deposit{
		Amount:         orderPost.Amount,
		AccountAddress: orderPost.Address,
	}

	err = c.db.Create(&deposit).Error
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.Response{
			Success:      false,
			ErrorMessage: errDBError.Error(),
		})
		return
	}

	// 创建支付宝订单
	var p = alipay.TradePagePay{}
	p.NotifyURL = c.config.Local.Nat + "/api/alipayNotify"
	p.ReturnURL = c.config.Local.Nat + "/api/alipayReturn"
	p.Subject = "充值"
	p.OutTradeNo = deposit.OrderNo
	p.TotalAmount = strconv.FormatFloat(deposit.Amount, 'f', 2, 64)
	p.ProductCode = "FAST_INSTANT_TRADE_PAY"

	url, err := c.alipayClient.TradePagePay(p)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.Response{
			Success:      false,
			ErrorMessage: errOrderError.Error(),
		})
		return
	}
	deposit.URL = url.String()

	// 返回结果
	ctx.JSON(http.StatusOK, models.Response{
		Success: true,
		Data:    deposit,
	})
}

func (c *Controller) GetDeposits(ctx *gin.Context) {
	itemQuery := models.DepositsQuery{}
	err := ctx.ShouldBindWith(&itemQuery, binding.Query)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	// 条件查询
	tx := c.db.Preload("Account").
		Limit(int(itemQuery.PageSize)).Offset(int((itemQuery.Current - 1) * itemQuery.PageSize)).
		Where(&models.Deposit{AccountAddress: itemQuery.AccountAddress})

	var deposits []models.Deposit
	err = tx.Find(&deposits).Error
	if err != nil {
		ctx.JSON(200, models.NewErrorCollectionResponse(err.Error()))
		return
	}

	ctx.JSON(200, models.NewSuccessCollectionResponse(deposits))
}

// AlipayNotify 接收支付宝页面支付结果
func (c *Controller) AlipayNotify(ctx *gin.Context) {

	// 接收返回参数
	notification, err := c.alipayClient.GetTradeNotification(ctx.Request)
	if err != nil {
		return
	}
	c.alipayClient.AckNotification(ctx.Writer)
	// 查找Deposit记录
	deposit := models.Deposit{OrderNo: notification.OutTradeNo}
	err = c.db.Where(&deposit).First(&deposit).Error
	if err != nil {
		return
	}

	// 调用合约
	tx, err := c.exchange.CurrencyToEth(common.HexToAddress(deposit.AccountAddress), deposit.Amount, deposit.OrderNo)
	if err != nil {
		return
	}
	// 更新Order
	err = c.db.Updates(models.Deposit{ID: deposit.ID, TxHash: tx.Hash().String(), Status: 1}).Error
	if err != nil {
		return
	}
}

func (c *Controller) AlipayReturn(ctx *gin.Context) {
	err := ctx.Request.ParseForm()
	if err != nil {
		return
	}
	ok, err := c.alipayClient.VerifySign(ctx.Request.Form)
	if ok {
		ctx.Redirect(http.StatusMovedPermanently, "http://localhost:8000")
	}
}
