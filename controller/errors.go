package controller

import "errors"

var (
	errPostRequestParamsLacking = errors.New("Post请求缺少参数 ")
	errPostRequestFileLacking   = errors.New("Post请求缺少上传文件 ")
	errParams                   = errors.New("参数错误")

	errSignature       = errors.New("签名错误")
	errAddress         = errors.New("区块链账户地址格式错误")
	errAccountNotFound = errors.New("未找到用户")
	errFileExist       = errors.New("文件已存在")
	errFileNotExist    = errors.New("文件不存在")
	errFileType        = errors.New("文件类型错误")
	errNotOwner        = errors.New("作品拥有者错误")
	errDBError         = errors.New("数据库错误")
	errOrderError      = errors.New("订单创建失败")
)
