package controller

import (
	"gitee.com/aqchain/backend/models"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"gorm.io/gorm"
	"net/http"
)

func (c *Controller) GetFileStorages(ctx *gin.Context) {

}

func (c *Controller) GetSales(ctx *gin.Context) {
	itemQuery := models.SaleQuery{}
	err := ctx.MustBindWith(&itemQuery, binding.Query)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	// 条件查询
	tx := c.db.Preload("Block").
		Limit(int(itemQuery.PageSize)).Offset(int((itemQuery.Current - 1) * itemQuery.PageSize)).
		Where(&models.Sale{FileHash: itemQuery.FileHash})

	var items []models.Sale
	err = tx.Find(&items).Error
	if err != nil {
		ctx.JSON(200, models.NewErrorCollectionResponse(err.Error()))
		return
	}

	ctx.JSON(200, models.NewSuccessCollectionResponse(items))
}

func (c *Controller) GetAuction(ctx *gin.Context) {
	auctionURI := models.AuctionURI{}
	err := ctx.BindUri(&auctionURI)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	auction := models.Auction{TxHash: auctionURI.TxHash}
	err = c.db.Preload("Block").Where(&auction).First(&auction).Error

	if err != nil {
		if err != gorm.ErrRecordNotFound {
			ctx.JSON(http.StatusInternalServerError, models.NewErrorResponse(errDBError.Error()))
			return
		} else {
			// 返回结果
			ctx.JSON(http.StatusOK, models.NewSuccessResponse(nil))
			return
		}
	}

	// 返回结果
	ctx.JSON(http.StatusOK, models.NewSuccessResponse(auction))
}

func (c *Controller) GetAuctions(ctx *gin.Context) {
	auctionQuery := models.AuctionQuery{}
	err := ctx.MustBindWith(&auctionQuery, binding.Query)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.NewErrorResponse(errParams.Error()))
		return
	}

	// 条件查询
	tx := c.db.Preload("Block").
		Where(&models.Auction{FileHash: auctionQuery.FileHash, CreateTxHash: auctionQuery.CreateTxHash, Type: auctionQuery.Type})

	if auctionQuery.PageSize > 0 && auctionQuery.Current > 0 {
		tx = tx.Limit(int(auctionQuery.PageSize)).Offset(int((auctionQuery.Current - 1) * auctionQuery.PageSize))
	}

	var auctions []models.Auction
	err = tx.Find(&auctions).Error
	if err != nil {
		ctx.JSON(200, models.NewErrorCollectionResponse(err.Error()))
		return
	}

	ctx.JSON(200, models.NewSuccessCollectionResponse(auctions))
}
