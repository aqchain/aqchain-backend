package config

import (
	"github.com/spf13/viper"
)

var (
	TempDir          = "temp"
	LogoDir          = "logo"
	ItemDir          = "item"
	CertificationDir = "certification"
)

// DatabaseConfig 数据库配置
type DatabaseConfig struct {
	DSN     string `json:"dsn"`     // 数据库连接配置
	Develop bool   `json:"develop"` // 是否为开发环境
}

// ServerConfig 服务配置
type ServerConfig struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

type EthereumConfig struct {
	Client   ClientConfig   `json:"client"`
	Keystore KeystoreConfig `json:"keystore"`
}

type ClientConfig struct {
	RPCAddress          string `json:"rpcAddress"`
	FileStorageContract string `json:"fileStorageContract"`
	SaleContract        string `json:"saleContract"`
	AuctionContract     string `json:"auctionContract"`
	LicenseContract     string `json:"licenseContract"`
	TokenContract       string `json:"tokenContract"`
	ExchangeContract    string `json:"exchangeContract"`
}

type KeystoreConfig struct {
	PrivateKey string `json:"privateKey"`
}

type LocalConfig struct {
	TempDir string `json:"tempDir"`
	Nat     string `json:"nat"`
}

type AlipayConfig struct {
	PrivateKey       string `json:"privateKey"`
	AppId            string `json:"appId"`
	AliPayRootCert   string `json:"aliPayRootCert"`
	AliPayPublicCert string `json:"aliPayPublicCert"`
	AppPublicCert    string `json:"appPublicCert"`
}

// Config 项目配置
type Config struct {
	Database DatabaseConfig
	Server   ServerConfig
	Ethereum EthereumConfig
	Local    LocalConfig
	Alipay   AlipayConfig
}

func New(path, filename string) (cfg *Config, err error) {
	config := viper.New()
	config.AddConfigPath(path)
	config.SetConfigFile(filename)

	if err = config.ReadInConfig(); err != nil {
		return cfg, err
	}

	if err = config.Unmarshal(&cfg); err != nil {
		return cfg, err
	}
	return
}
