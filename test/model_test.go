package test

import (
	"gitee.com/aqchain/backend/config"
	"gitee.com/aqchain/backend/database"
	"gitee.com/aqchain/backend/models"
	"gitee.com/aqchain/backend/utils"
	"testing"
)

func TestName(t *testing.T) {

	db, err := database.Database(config.DatabaseConfig{
		DSN:     "root:123456@tcp(127.0.0.1:3306)/aqchain?charset=utf8mb4&parseTime=True&loc=Local",
		Develop: true,
	})
	utils.Fatal(err)

	db = db.Begin()

	account := models.Account{
		Address:  "0x001",
		Username: "test",
	}
	err = db.Create(&account).Error
	utils.Fatal(err)

	item := models.Item{
		FileHash:       "0x100",
		Name:           "test",
		Category:       1,
		FileName:       "test",
		Status:         0,
		CreatorAddress: "0x001",
		OwnerAddress:   "0x001",
	}

	err = db.Omit("FileStorageTxHash").Create(&item).Error
	utils.Fatal(err)

	fileStorage := models.FileStorage{
		TxHash:      "0xPPP",
		FileHash:    "0x100",
		TokenID:     0,
		From:        "",
		To:          "",
		Type:        "",
		BlockNumber: 1,
	}

	item.FileStorage = fileStorage

	err = db.Omit("FileStorage.BlockNumber").Updates(&item).Error
	utils.Fatal(err)

	db.Commit()
}
