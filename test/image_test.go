package test

import (
	"fmt"
	"github.com/fogleman/gg"
	"testing"
)

func Test(t *testing.T) {
	im, err := gg.LoadImage("/home/ganyu/golandProject/aqchain-back/test/certificate.png")
	if err != nil {
		panic(err)
	}
	w := im.Bounds().Size().X
	h := im.Bounds().Size().Y

	dc := gg.NewContext(w, h)
	dc.DrawImage(im, 0, 0)
	dc.SetRGB(0, 0, 0)
	if err := dc.LoadFontFace("/home/ganyu/golandProject/aqchain-back/test/方正粗黑宋简体.ttf", 50); err != nil {
		panic(err)
	}
	dc.DrawStringAnchored("数字内容存证证明", float64(w/2), 200, 0.5, 0.5)

	if err := dc.LoadFontFace("/home/ganyu/golandProject/aqchain-back/test/方正粗黑宋简体.ttf", 20); err != nil {
		panic(err)
	}

	boldText := []string{
		"编号：BC00000000000000000000000001",
		"存证账户",
		"存证数据类型",
		"数据哈希值",
		"统一证据编号",
		"数据名称",
		"数据大小",
	}
	dc.DrawStringAnchored(boldText[0], float64(w/2), 250, 0.5, 0.5)
	for i := 1; i < len(boldText); i++ {
		dc.DrawStringAnchored(boldText[i], 200, float64(350+50*i), 0.5, 0.5)
	}

	if err := dc.LoadFontFace("/home/ganyu/golandProject/aqchain-back/test/方正粗黑宋简体.ttf", 15); err != nil {
		panic(err)
	}
	normalText := []string{
		"甘宇",
		"图片文件",
		"0x00000000000000000000000000000000000000001",
		"0x00000000000000000000000000000000000000001",
		"存储的图片.png",
		"1111112222222byte",
	}

	for i := 0; i < len(boldText)-1; i++ {
		dc.DrawStringAnchored(normalText[i], 480, float64(400+50*i), 0.5, 0.5)
	}

	text := "2020年01月01日01时01分01秒接收到安权链区块存证信息，该存证事件包括以下信息，特此证明"
	dc.DrawStringWrapped(text, 200, 320, 0.5, 0.5, 200, 1.5, gg.AlignLeft)

	dc.SavePNG("test.png")

}

func TestImages(t *testing.T) {
	im, err := gg.LoadImage("/home/ganyu/图片/测试图片1-1")
	if err != nil {
		panic(err)
	}
	w := im.Bounds().Size().X
	h := im.Bounds().Size().Y

	dc := gg.NewContext(w, h)
	dc.DrawImage(im, 0, 0)
	dc.SetRGB(0, 0, 0)
	if err := dc.LoadFontFace("方正粗黑宋简体.ttf", 50); err != nil {
		panic(err)
	}

	for i := 1; i < 21; i++ {
		for j := 1; j < 4; j++ {
			dc.DrawStringAnchored(fmt.Sprintf("测试图片%d-%d", i, j), float64(w/2), 200, 0.5, 0.5)
			dc.SavePNG(fmt.Sprintf("/home/ganyu/图片/测试图片%d-%d.png", i, j))
		}
	}
}
