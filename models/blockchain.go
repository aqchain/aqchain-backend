package models

type Block struct {
	Number       uint64        `json:"number" gorm:"primaryKey"` // 区块高度
	Time         uint64        `json:"time"`                     // 区块时间
	Hash         string        `json:"hash"`                     // 区块哈希
	TxHash       string        `json:"txHash"`
	Coinbase     string        `json:"coinbase"` // 出块节点地址
	Size         float64       `json:"size"`
	Transactions []Transaction `json:"transactions"`
}

type Transaction struct {
	ID          uint    `json:"id" gorm:"primaryKey"`
	Hash        string  `json:"hash"`
	BlockNumber uint64  `json:"blockNumber"`
	From        string  `json:"from"`
	To          string  `json:"to"`
	Size        float64 `json:"size"`
	Cost        uint64  `json:"cost"`
}
