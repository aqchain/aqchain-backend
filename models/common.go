package models

import (
	"reflect"
)

func NewErrorResponse(msg string) Response {
	return Response{
		Success:      false,
		ErrorMessage: msg,
	}
}

func NewSuccessResponse(data interface{}) Response {
	return Response{
		Success:      true,
		ErrorMessage: "",
		Data:         data,
	}
}

func NewSuccessCollectionResponse(data interface{}) CollectionResponse {
	switch reflect.TypeOf(data).Kind() {
	case reflect.Array, reflect.Slice:
		arr := reflect.ValueOf(data)
		return CollectionResponse{
			Response: Response{
				Success: true,
				Data:    data,
			},
			Total: arr.Len(),
		}
	}
	return CollectionResponse{}
}

func NewErrorCollectionResponse(msg string) CollectionResponse {
	return CollectionResponse{
		Response: Response{
			Success:      false,
			ErrorMessage: msg,
		},
	}
}

type Response struct {
	Success      bool        `json:"success"`
	ErrorMessage string      `json:"errorMessage,omitempty"`
	Data         interface{} `json:"data,omitempty"`
}

type Pagination struct {
	PageSize uint `form:"pageSize" binding:"-" query:"pageSize" json:"pageSize" example:"20"`
	Current  uint `form:"current" binding:"-" query:"current" json:"current" example:"1"`
}

func (r Response) data() interface{} {
	return r.Data
}

type CollectionResponse struct {
	Response
	Total int `json:"total,omitempty"`
}

func (r CollectionResponse) data() interface{} {
	return r.Data
}
