package models

const AccountMessage = "Account Operation Sign Message"

type AccountGet struct {
	Address string `uri:"address" binding:"required" path:"address" json:"address" required:"true" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
}

type AccountPost struct {
	Signature  string `json:"signature" binding:"required" required:"true" example:"0x1fdbee3be86c3dd83eaed068523b8fce52c9f1bbb6f47e8b6f3c3ef010e1e33e1d14e4d376128fd0ffe64e6f9f3e50bab99e934b5a908982b39ebdf4c8479a081c"`
	Address    string `json:"address" binding:"required" required:"true" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
	Mail       string `json:"mail" binding:"required" example:"330596160@qq.com"`
	Username   string `json:"username" binding:"required" example:"ganyu"`
	AlipayID   string `json:"alipayID" binding:"-" example:"axmyor6190@sandbox.com"`
	AlipayName string `json:"alipayName" binding:"-" example:"axmyor6190"`
}

type AccountPut AccountPost

type Account struct {
	ID uint `json:"id" gorm:"primarykey"` // 数据库主键

	Address    string `json:"address" required:"true" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4" gorm:"index:,unique"`
	Mail       string `json:"mail" example:"330596160@qq.com"` // 邮箱地址
	Username   string `json:"username" example:"ganyu"`        // 用户名
	Avatar     string `json:"avatar" example:"https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png"`
	AlipayID   string `json:"alipayID" example:"axmyor6190@sandbox.com"`
	AlipayName string `json:"alipayName" example:"axmyor6190"`

	Nonce uint `json:"nonce" example:"1"`
}

type AccountResponse struct {
	Response
	Data Account `json:"data,omitempty"`
}

func (r AccountResponse) data() interface{} {
	return r.Data
}
