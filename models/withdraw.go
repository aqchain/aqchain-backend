package models

import (
	"fmt"
	"gorm.io/gorm"
	"time"
)

const WithdrawSignMessage = "Withdraw Operation Sign Message"

type WithdrawQuery struct {
	Pagination
	AccountAddress string `json:"accountAddress" form:"accountAddress" binding:"required" query:"accountAddress" required:"true" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
	Type           string `json:"type" form:"type" binding:"-" query:"type" example:"alipay"`
}

type WithdrawPost struct {
	Signature string  `json:"signature" binding:"required" required:"true" example:"0x1fdbee3be86c3dd83eaed068523b8fce52c9f1bbb6f47e8b6f3c3ef010e1e33e1d14e4d376128fd0ffe64e6f9f3e50bab99e934b5a908982b39ebdf4c8479a081c"`
	Address   string  `json:"address" binding:"required" required:"true" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
	Amount    float64 `json:"amount" binding:"required" required:"true" example:"100.00"`
	Type      string  `json:"type" binding:"required" required:"true" example:"alipay"`
}

type WithdrawResponse struct {
	Response
	Data Withdraw `json:"data,omitempty"`
}

type WithdrawCollectionResponse struct {
	Response
	Data []Withdraw `json:"data,omitempty"`
}

type Withdraw struct {
	ID        uint      `json:"id" required:"true" gorm:"primarykey" ` // 数据库主键
	CreatedAt time.Time `json:"createdAt" required:"true"`             // 创建时间
	OrderNo   string    `json:"orderNo" required:"true" gorm:"index"`

	AccountAddress string  `json:"accountAddress" required:"true" gorm:"index"`
	Account        Account `json:"account,omitempty" gorm:"references:Address;foreignKey:AccountAddress"`

	Type   string  `json:"type" required:"true"`   // 提现方式 目前只完成支付宝
	Amount float64 `json:"amount" required:"true"` // 订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
	TxHash string  `json:"txHash" required:"true"` // 调用合约的交易
	Status uint8   `json:"status" required:"true"` // 0 创建 1 完成
}

func (w *Withdraw) AfterCreate(tx *gorm.DB) (err error) {
	w.OrderNo = fmt.Sprintf("%s%d", w.CreatedAt.Format("20060102150405"), w.ID)
	err = tx.Model(w).Select("OrderNo").Updates(w).Error
	return
}
