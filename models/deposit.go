package models

import (
	"fmt"
	"gorm.io/gorm"
	"time"
)

const DepositSignMessage = "Deposit Operation Sign Message"

type DepositsQuery struct {
	Pagination
	AccountAddress string `json:"accountAddress" form:"accountAddress" binding:"required" query:"accountAddress" required:"true" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
}

type DepositPost struct {
	Signature string  `json:"signature" binding:"required" required:"true" example:"0x1fdbee3be86c3dd83eaed068523b8fce52c9f1bbb6f47e8b6f3c3ef010e1e33e1d14e4d376128fd0ffe64e6f9f3e50bab99e934b5a908982b39ebdf4c8479a081c"`
	Address   string  `json:"address" binding:"required" required:"true" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
	Amount    float64 `json:"amount" binding:"required" required:"true" example:"100.00"`
	Type      string  `json:"type" binding:"required" required:"true" example:"alipay"`
}

type DepositResponse struct {
	Response
	Data Deposit `json:"data,omitempty"`
}

type DepositCollectionResponse struct {
	Response
	Data []Deposit `json:"data,omitempty"`
}

type Deposit struct {
	ID        uint      `json:"id" required:"true" gorm:"primarykey" ` // 数据库主键
	CreatedAt time.Time `json:"createdAt" required:"true"`             // 创建时间
	OrderNo   string    `json:"orderNo" required:"true" gorm:"index"`

	AccountAddress string  `json:"accountAddress" required:"true" gorm:"index"`
	Account        Account `json:"account,omitempty" gorm:"references:Address;foreignKey:AccountAddress"`

	Type   string  `json:"type" required:"true"`   // 提现方式 目前只完成支付宝
	Amount float64 `json:"amount" required:"true"` // 订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
	TxHash string  `json:"txHash" required:"true"`
	Status uint8   `json:"status" required:"true"` // 0 创建 1

	URL string `json:"url" gorm:"-"`
}

func (d *Deposit) AfterCreate(tx *gorm.DB) (err error) {
	d.OrderNo = fmt.Sprintf("%s%d", d.CreatedAt.Format("20060102150405"), d.ID)
	err = tx.Model(d).Select("OrderNo").Updates(d).Error
	return
}
