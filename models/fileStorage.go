package models

type FileStorageQuery struct {
	Pagination
	FileHash string `form:"fileHash" binding:"required" query:"fileHash" required:"true" json:"fileHash"`
}

type FileStorage struct {
	ID uint `json:"id" gorm:"primarykey"` // 数据库主键

	TxHash   string `json:"txHash" gorm:"index;not null;unique;"`
	FileHash string `json:"fileHash" gorm:"index;not null;"`
	TokenID  uint64 `json:"tokenID"`

	From string `json:"from"`
	To   string `json:"to"`

	Type string `json:"type" gorm:"type:varchar(10)"` // 类型 0:create 1:transfer 2:approval

	BlockNumber uint64 `json:"blockNumber"` // 区块高度
	Block       Block  `json:"block"`       // 区块高度
}

type FileStorageResponse struct {
	Response
	Data FileStorage `json:"data,omitempty"`
}

type FileStoragesResponse struct {
	CollectionResponse
	Data []FileStorage `json:"data,omitempty"`
}
