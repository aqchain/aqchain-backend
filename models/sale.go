package models

type SaleQuery struct {
	Pagination
	FileHash string `form:"fileHash" binding:"required" query:"fileHash" required:"true" json:"fileHash"`
}

type Sale struct {
	ID uint `json:"id" gorm:"primarykey"` // 数据库主键

	TxHash   string `json:"txHash" gorm:"index;not null;unique;"`
	FileHash string `json:"fileHash" gorm:"index;not null;"`
	TokenID  uint64 `json:"tokenID"`

	Seller string `json:"seller"`
	Buyer  string `json:"buyer"`
	Price  uint64 `json:"price"`

	Type string `json:"type" gorm:"type:varchar(10)"` // 类型 0:create 1:update 2:delete 3:confirm

	BlockNumber uint64 `json:"blockNumber"` // 区块高度
	Block       Block  `json:"block"`       // 区块高度
}

type SaleResponse struct {
	Response
	Data Sale `json:"data,omitempty"`
}

type SalesResponse struct {
	CollectionResponse
	Data []Sale `json:"data,omitempty"`
}
