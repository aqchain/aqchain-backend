package models

type AuctionURI struct {
	TxHash string `uri:"txHash" binding:"required" path:"txHash" required:"true" json:"txHash"`
}

type AuctionQuery struct {
	Pagination
	FileHash     string `form:"fileHash" binding:"required" query:"fileHash" required:"true" json:"fileHash"`
	CreateTxHash string `form:"createTxHash" binding:"-" query:"createTxHash" required:"false" json:"createTxHash"`
	Type         string `form:"type" binding:"-" query:"type" required:"false" json:"type"`
}

type Auction struct {
	ID uint `json:"id" gorm:"primarykey"` // 数据库主键

	TxHash   string `json:"txHash" gorm:"index;not null;unique;"`
	FileHash string `json:"fileHash" gorm:"index;not null;"`
	TokenID  uint64 `json:"tokenID"`

	Seller string `json:"seller"`
	Bidder string `json:"bidder"`
	Price  uint64 `json:"price"`
	Begin  uint64 `json:"begin"`
	End    uint64 `json:"end"`

	Type         string `json:"type" gorm:"type:varchar(10)"`         // 类型 0:create 1:bid 2:withdraw 3:finish
	CreateTxHash string `json:"createTxHash"  gorm:"index;not null;"` // 创建拍卖的交易

	BlockNumber uint64 `json:"blockNumber"` // 区块高度
	Block       Block  `json:"block"`       // 区块高度
}

type AuctionResponse struct {
	Response
	Data Auction `json:"data,omitempty"`
}

type AuctionsResponse struct {
	CollectionResponse
	Data []Auction `json:"data,omitempty"`
}
