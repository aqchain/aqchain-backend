package models

import (
	"mime/multipart"
	"time"
)

const ItemSignMessage = "Item Operation Sign Message"

type ItemGet struct {
	FileHash string `uri:"fileHash" binding:"required" path:"fileHash" example:"0x82fb0efce9c4e3d416bbca06467445fccc3ab9148aac459a5e9506551fc21fc0" json:"fileHash"`
}

type ItemPost struct {
	Signature   string                `form:"signature" binding:"required" formData:"signature" required:"true" example:"0xb61f05a0f610179bcba3d706c173cf41259762bd483242a318c370fe4b5db8b228b045faf2f0f7e1e87276f478193a3ccb88ec972a5a2c4d874c3367368730a01c"`
	Address     string                `form:"address" binding:"required" formData:"address" required:"true" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
	Name        string                `form:"name" binding:"required" formData:"name" required:"true" example:"作品名称"`
	Description string                `form:"description" formData:"description" example:"作品介绍"`
	Category    uint8                 `form:"category" binding:"required" formData:"category" required:"true" example:"1"`
	FileName    string                `form:"fileName" binding:"required" formData:"fileName" required:"true" example:"test.png"` // 上传文件名称
	FileSize    uint64                `form:"fileSize" binding:"required" formData:"fileSize" required:"true" example:"10000000"` // 上传文件大小
	FileType    string                `form:"fileType" binding:"required" formData:"fileType" required:"true" example:"png"`      // 上传文件类型
	File        *multipart.FileHeader `formData:"file" required:"true"`
}

type ItemPut struct {
	FileHash    string `binding:"required" json:"fileHash" example:"0x82fb0efce9c4e3d416bbca06467445fccc3ab9148aac459a5e9506551fc21fc0" gorm:"index;not null"` // 上传文件哈希值
	Signature   string `binding:"required" json:"signature" example:"0xb61f05a0f610179bcba3d706c173cf41259762bd483242a318c370fe4b5db8b228b045faf2f0f7e1e87276f478193a3ccb88ec972a5a2c4d874c3367368730a01c"`
	Address     string `binding:"required" json:"address" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
	Name        string `json:"name" example:"作品名称"`
	Description string `json:"description" example:"作品介绍"`
	TxHash      string `json:"txHash" example:"0xecd9602c07e6c2cb10fadef7d41f93dd443f5112c5acf128c8bdb7da38356fda"`
}

type ItemsQuery struct {
	Pagination
	CreatorAddress string `form:"creatorAddress" binding:"-" query:"creatorAddress" json:"creatorAddress" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
	OwnerAddress   string `form:"ownerAddress" binding:"-" query:"ownerAddress" json:"ownerAddress" example:"0xfC97a1A683FB0108299D5725BC9d72899C9A86B4"`
	Approved       string `form:"approved" binding:"-" query:"approved" json:"approved" example:"0x12a0674e1ba219A2FF793aEF870779F4ce75Ff35"`
	Status         uint8  `form:"status" binding:"-" query:"status" json:"status" example:"2"`
	Name           string `form:"name" binding:"-" query:"name" json:"name" example:"作品名称"`
	Category       uint8  `form:"category" binding:"-" query:"category" json:"category" example:"2"`
}

type ItemResponse struct {
	Response
	Data Item `json:"data,omitempty"`
}

func (r ItemResponse) data() interface{} {
	return r.Data
}

type ItemsResponse struct {
	CollectionResponse
	Data []Item `json:"data,omitempty"`
}

// Item 代表单个作品
type Item struct {
	ID        uint      `json:"id" gorm:"primarykey" example:"1"` // 数据库主键
	CreatedAt time.Time `json:"createdAt"`                        // 创建时间

	FileHash string `json:"fileHash" example:"0x82fb0efce9c4e3d416bbca06467445fccc3ab9148aac459a5e9506551fc21fc0" gorm:"index;not null;unique;"` // 上传文件哈希值

	Name        string `json:"name" required:"true"`     // 作品名称
	Description string `json:"description"`              // 作品描述
	Category    uint8  `json:"category" required:"true"` // 作品类型

	FileName string `json:"fileName" required:"true"` // 上传文件名称
	FileSize uint64 `json:"fileSize" required:"true"` // 上传文件大小
	FileType string `json:"fileType" required:"true"` // 上传文件类型

	Status uint8 `json:"status" required:"true"` // 0 完成上传 1 完成区块链确权 2 定价交易 3 拍卖 4 授权

	ApprovalTxHash string `json:"approvalTxHash"` //

	FileStorageTxHash string      `json:"fileStorageTxHash" required:"true" gorm:"index"`
	FileStorage       FileStorage `json:"fileStorage,omitempty" gorm:"references:TxHash;foreignKey:FileStorageTxHash"`
	SaleTxHash        string      `json:"saleTxHash" gorm:"index"`                                       // 交易hash 当处于交易状态时 记录对应的最新操作记录的TxHash
	Sale              Sale        `json:"sale,omitempty" gorm:"references:TxHash;foreignKey:SaleTxHash"` // 交易hash 当处于交易状态时 记录对应的最新操作记录的TxHash
	AuctionTxHash     string      `json:"auctionTxHash" gorm:"index"`                                    // 交易hash 当处于交易状态时 记录对应的最新操作记录的TxHash
	Auction           Auction     `json:"auction,omitempty" gorm:"references:TxHash;foreignKey:AuctionTxHash"`

	CreatorAddress string  `json:"creatorAddress" required:"true" gorm:"index;not null"` // 作品创建者的外键
	Creator        Account `json:"creator,omitempty" gorm:"references:Address;foreignKey:CreatorAddress"`

	OwnerAddress string  `json:"ownerAddress" required:"true" gorm:"index;not null"`                // 作品当前拥有者的外键
	Owner        Account `json:"owner,omitempty" gorm:"references:Address;foreignKey:OwnerAddress"` // 作品当前拥有者
}
