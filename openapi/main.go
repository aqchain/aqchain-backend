package main

import (
	"fmt"
	"gitee.com/aqchain/backend/models"
	"github.com/swaggest/openapi-go/openapi3"
	"log"
	"net/http"
	"os"
	"path"
)

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	reflector := openapi3.Reflector{}
	reflector.Spec = &openapi3.Spec{Openapi: "3.0.3"}
	reflector.Spec.Info.
		WithTitle("安权链 API").
		WithVersion("0.0.1").
		WithDescription("安权链 API 文档 使用openapi3")

	serverLocal := openapi3.Server{}
	serverLocal.WithURL("http://127.0.0.1:8090")
	serverLocal.WithDescription("本地测试环境")

	serverCloud := openapi3.Server{}
	serverCloud.WithURL("http://aqchain.xyz:8090")
	serverCloud.WithDescription("腾讯云测试环境")

	reflector.Spec.WithServers(serverLocal, serverCloud)

	//httpSecurityScheme := openapi3.HTTPSecurityScheme{}
	//httpSecurityScheme.WithScheme("bearer")
	//httpSecurityScheme.WithBearerFormat("JWT")
	//securityScheme := openapi3.SecurityScheme{}
	//securityScheme.WithHTTPSecurityScheme(httpSecurityScheme)
	//securitySchemeOrRef := openapi3.SecuritySchemeOrRef{}
	//securitySchemeOrRef.WithSecurityScheme(securityScheme)
	//componentsSecuritySchemes := openapi3.ComponentsSecuritySchemes{}
	//componentsSecuritySchemes.WithMapOfSecuritySchemeOrRefValuesItem("bearerAuth", securitySchemeOrRef)
	//components := openapi3.Components{}
	//components.WithSecuritySchemes(componentsSecuritySchemes)
	//reflector.Spec.WithComponents(components)

	postAccountOp := openapi3.Operation{}
	postAccountOp.WithTags("account")
	postAccountOp.WithSummary("钱包账户信息创建")
	postAccountOp.WithDescription("钱包账户信息创建")
	postAccountOp.WithID("post-account")
	handleError(reflector.SetRequest(&postAccountOp, new(models.AccountPost), http.MethodPost))
	handleError(reflector.SetJSONResponse(&postAccountOp, new(models.AccountResponse), http.StatusOK))
	handleError(reflector.SetJSONResponse(&postAccountOp, new(models.AccountResponse), http.StatusBadRequest))
	handleError(reflector.SetJSONResponse(&postAccountOp, new(models.AccountResponse), http.StatusInternalServerError))
	handleError(reflector.Spec.AddOperation(http.MethodPost, "/api/account", postAccountOp))

	getAccountOp := openapi3.Operation{}
	getAccountOp.WithTags("account")
	getAccountOp.WithSummary("钱包账户信息获取")
	getAccountOp.WithDescription("钱包账户信息获取")
	getAccountOp.WithID("get-account")
	handleError(reflector.SetRequest(&getAccountOp, new(models.AccountGet), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getAccountOp, new(models.AccountResponse), http.StatusOK))
	handleError(reflector.SetJSONResponse(&getAccountOp, new(models.AccountResponse), http.StatusBadRequest))
	handleError(reflector.SetJSONResponse(&getAccountOp, new(models.AccountResponse), http.StatusInternalServerError))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/account/{address}", getAccountOp))

	putAccountOp := openapi3.Operation{}
	putAccountOp.WithTags("account")
	putAccountOp.WithSummary("钱包账户信息修改")
	putAccountOp.WithDescription("钱包账户信息修改")
	putAccountOp.WithID("put-account")
	handleError(reflector.SetRequest(&putAccountOp, new(models.AccountPut), http.MethodPut))
	handleError(reflector.SetJSONResponse(&putAccountOp, new(models.AccountResponse), http.StatusOK))
	handleError(reflector.SetJSONResponse(&putAccountOp, new(models.AccountResponse), http.StatusBadRequest))
	handleError(reflector.SetJSONResponse(&putAccountOp, new(models.AccountResponse), http.StatusInternalServerError))
	handleError(reflector.Spec.AddOperation(http.MethodPut, "/api/account", putAccountOp))

	addItemOp := openapi3.Operation{}
	addItemOp.WithTags("item")
	addItemOp.WithSummary("作品创建")
	addItemOp.WithDescription("作品创建")
	addItemOp.WithID("post-item")
	handleError(reflector.SetRequest(&addItemOp, new(models.ItemPost), http.MethodPost))
	handleError(reflector.SetJSONResponse(&addItemOp, new(models.ItemResponse), http.StatusOK))
	handleError(reflector.SetJSONResponse(&addItemOp, new(models.ItemResponse), http.StatusBadRequest))
	handleError(reflector.SetJSONResponse(&addItemOp, new(models.ItemResponse), http.StatusInternalServerError))
	handleError(reflector.Spec.AddOperation(http.MethodPost, "/api/item", addItemOp))

	getItemOp := openapi3.Operation{}
	getItemOp.WithTags("item")
	getItemOp.WithSummary("作品信息获取")
	getItemOp.WithDescription("作品信息获取")
	getItemOp.WithID("get-item")
	handleError(reflector.SetRequest(&getItemOp, new(models.ItemGet), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getItemOp, new(models.ItemResponse), http.StatusOK))
	handleError(reflector.SetJSONResponse(&getItemOp, new(models.ItemResponse), http.StatusBadRequest))
	handleError(reflector.SetJSONResponse(&getItemOp, new(models.ItemResponse), http.StatusInternalServerError))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/item/{fileHash}", getItemOp))

	putItemOp := openapi3.Operation{}
	putItemOp.WithTags("item")
	putItemOp.WithSummary("作品信息修改")
	putItemOp.WithDescription("作品信息修改")
	putItemOp.WithID("put-item")
	handleError(reflector.SetRequest(&putItemOp, new(models.ItemPut), http.MethodPut))
	handleError(reflector.SetJSONResponse(&putItemOp, new(models.ItemResponse), http.StatusOK))
	handleError(reflector.Spec.AddOperation(http.MethodPut, "/api/item", putItemOp))

	getItemsOp := openapi3.Operation{}
	getItemsOp.WithTags("item")
	getItemsOp.WithSummary("作品信息集合获取")
	getItemsOp.WithDescription("作品信息集合获取")
	getItemsOp.WithID("get-items")
	handleError(reflector.SetRequest(&getItemsOp, new(models.ItemsQuery), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getItemsOp, new(models.ItemsResponse), http.StatusOK))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/items/", getItemsOp))

	getFileOp := openapi3.Operation{}
	getFileOp.WithTags("file")
	getFileOp.WithSummary("文件获取")
	getFileOp.WithDescription("文件获取")
	getFileOp.WithID("get-file")
	handleError(reflector.SetRequest(&getFileOp, new(models.ItemGet), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getFileOp, new(string), http.StatusOK))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/file/{fileHash}", getFileOp))

	getFileStoragesOp := openapi3.Operation{}
	getFileStoragesOp.WithTags("trade")
	getFileStoragesOp.WithSummary("作品交易记录获取")
	getFileStoragesOp.WithDescription("作品交易记录获取")
	getFileStoragesOp.WithID("get-fileStorages")
	handleError(reflector.SetRequest(&getFileStoragesOp, new(models.FileStorageQuery), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getFileStoragesOp, new(models.FileStoragesResponse), http.StatusOK))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/fileStorages/", getFileStoragesOp))

	getSalesOp := openapi3.Operation{}
	getSalesOp.WithTags("trade")
	getSalesOp.WithSummary("作品交易记录获取")
	getSalesOp.WithDescription("作品交易记录获取")
	getSalesOp.WithID("get-sales")
	handleError(reflector.SetRequest(&getSalesOp, new(models.SaleQuery), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getSalesOp, new(models.SalesResponse), http.StatusOK))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/sales/", getSalesOp))

	getAuctionOp := openapi3.Operation{}
	getAuctionOp.WithTags("trade")
	getAuctionOp.WithSummary("作品交易记录获取")
	getAuctionOp.WithDescription("作品交易记录获取")
	getAuctionOp.WithID("get-auction")
	handleError(reflector.SetRequest(&getAuctionOp, new(models.AuctionURI), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getAuctionOp, new(models.Auction), http.StatusOK))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/auction/{txHash}", getAuctionOp))

	getAuctionsOp := openapi3.Operation{}
	getAuctionsOp.WithTags("trade")
	getAuctionsOp.WithSummary("作品交易记录获取")
	getAuctionsOp.WithDescription("作品交易记录获取")
	getAuctionsOp.WithID("get-auctions")
	handleError(reflector.SetRequest(&getAuctionsOp, new(models.AuctionQuery), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getAuctionsOp, new(models.AuctionsResponse), http.StatusOK))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/auctions/", getAuctionsOp))

	postDepositOp := openapi3.Operation{}
	postDepositOp.WithTags("deposit")
	postDepositOp.WithSummary("充值记录创建")
	postDepositOp.WithDescription("充值记录创建")
	postDepositOp.WithID("post-deposit")
	handleError(reflector.SetRequest(&postDepositOp, new(models.DepositPost), http.MethodPost))
	handleError(reflector.SetJSONResponse(&postDepositOp, new(models.DepositResponse), http.StatusOK))
	handleError(reflector.SetJSONResponse(&postDepositOp, new(models.DepositResponse), http.StatusBadRequest))
	handleError(reflector.SetJSONResponse(&postDepositOp, new(models.DepositResponse), http.StatusInternalServerError))
	handleError(reflector.Spec.AddOperation(http.MethodPost, "/api/deposit", postDepositOp))

	//getOrderOp := openapi3.Operation{}
	//getOrderOp.WithTags("order")
	//getOrderOp.WithSummary("订单获取")
	//getOrderOp.WithDescription("订单获取")
	//getOrderOp.WithID("get-order")
	//handleError(reflector.SetRequest(&getOrderOp, new(models.DepositPost), http.MethodGet))
	//handleError(reflector.SetJSONResponse(&getOrderOp, new(models.DepositResponse), http.StatusOK))
	//handleError(reflector.SetJSONResponse(&getOrderOp, new(models.DepositResponse), http.StatusBadRequest))
	//handleError(reflector.SetJSONResponse(&getOrderOp, new(models.DepositResponse), http.StatusInternalServerError))
	//handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/order/{id}", getOrderOp))

	getDepositsOp := openapi3.Operation{}
	getDepositsOp.WithTags("deposit")
	getDepositsOp.WithSummary("充值记录集合获取")
	getDepositsOp.WithDescription("充值记录集合获取")
	getDepositsOp.WithID("get-deposits")
	handleError(reflector.SetRequest(&getDepositsOp, new(models.DepositsQuery), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getDepositsOp, new(models.DepositCollectionResponse), http.StatusOK))
	handleError(reflector.SetJSONResponse(&getDepositsOp, new(models.DepositCollectionResponse), http.StatusBadRequest))
	handleError(reflector.SetJSONResponse(&getDepositsOp, new(models.DepositCollectionResponse), http.StatusInternalServerError))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/deposits", getDepositsOp))

	postWithdrawOp := openapi3.Operation{}
	postWithdrawOp.WithTags("withdraw")
	postWithdrawOp.WithSummary("提现记录创建")
	postWithdrawOp.WithDescription("提现记录创建")
	postWithdrawOp.WithID("post-withdraw")
	handleError(reflector.SetRequest(&postWithdrawOp, new(models.WithdrawPost), http.MethodPost))
	handleError(reflector.SetJSONResponse(&postWithdrawOp, new(models.WithdrawResponse), http.StatusOK))
	handleError(reflector.SetJSONResponse(&postWithdrawOp, new(models.WithdrawResponse), http.StatusBadRequest))
	handleError(reflector.SetJSONResponse(&postWithdrawOp, new(models.WithdrawResponse), http.StatusInternalServerError))
	handleError(reflector.Spec.AddOperation(http.MethodPost, "/api/withdraw", postWithdrawOp))

	getWithdrawsOp := openapi3.Operation{}
	getWithdrawsOp.WithTags("withdraw")
	getWithdrawsOp.WithSummary("充值记录集合获取")
	getWithdrawsOp.WithDescription("充值记录集合获取")
	getWithdrawsOp.WithID("get-withdraws")
	handleError(reflector.SetRequest(&getWithdrawsOp, new(models.DepositsQuery), http.MethodGet))
	handleError(reflector.SetJSONResponse(&getWithdrawsOp, new(models.DepositCollectionResponse), http.StatusOK))
	handleError(reflector.SetJSONResponse(&getWithdrawsOp, new(models.DepositCollectionResponse), http.StatusBadRequest))
	handleError(reflector.SetJSONResponse(&getWithdrawsOp, new(models.DepositCollectionResponse), http.StatusInternalServerError))
	handleError(reflector.Spec.AddOperation(http.MethodGet, "/api/withdraws", getWithdrawsOp))

	schema, err := reflector.Spec.MarshalJSON()
	if err != nil {
		log.Fatal(err)
	}

	wd, err := os.Getwd()
	if err != nil {
		return
	}
	err = os.WriteFile(path.Join(wd, "openapi", "openapi3.json"), schema, 0777)
	fmt.Println(string(schema))
}
